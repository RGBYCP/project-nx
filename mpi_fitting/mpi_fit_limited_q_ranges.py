from mpi4py import MPI
import numpy as np

import h5py
import os, sys, ast
import logging
import datetime

from time import time

import tialn_fitting_limited_q_ranges
from tialn_fitting_limited_q_ranges import *

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
num_procs = comm.Get_size()
pname = MPI.Get_processor_name()

if rank == 0:
    # --- create common data ---
    # Data file 
    sample_path='/data/visitors/nanomax/20190570/2019052208/process/'
    sample_file=sys.argv[1]

    source=sample_path+sample_file
    output_path=sample_path

    sys.stdout.flush()

    # Modification necessary due to script
    mod_output_path=output_path+sample_file
    output_path,file_name=os.path.split(mod_output_path)
    

    

    sys.stdout.flush()
    print("--------------------     Final output_path  --------------------")
    print('Path: ', output_path)

    # --- reading input data ---
    print("(rank=%d) Reading 1DPilatusData" % (rank,))
    file1=Read1DPilatusData(source,output_path)
    q_array,integration_array,sigma_array,azimuth_array=file1.data_read()
    print("(rank=%d) Reading 1DPilatusData DONE" % (rank,))
    
    
    q_min=float(sys.argv[8]) # 2.0
    q_max=float(sys.argv[9]) # 3.5
    
    # --- Fit1DPilatusData instance (later will be destroyed) ---
    fit1 = Fit1DPilatusData(source,output_path,q_array,integration_array,sigma_array,azimuth_array,q_min=q_min,q_max=q_max)
    
    # --- irows selection ---- 
    #x_start=20
    #x_end=50
    #y_start=20
    #y_end=50
    #x_len=71
    
    #x_start=int(sys.argv[2]) # 47
    #x_end=int(sys.argv[3]) # 51
    #y_start=int(sys.argv[4]) # 47
    #y_end=int(sys.argv[5]) # 51
    #x_len=int(sys.argv[6]) # 71
    #irows = np.asarray(fit1.get_irows(x_start, x_end, y_start, y_end,  x_len))
    #irows_index = np.arange(irows.size)
    
    start_row=int(sys.argv[2]) # 47
    start_col=int(sys.argv[3]) # 51
    rows=int(sys.argv[4]) # 47
    cols=int(sys.argv[5]) # 51
    col_len=int(sys.argv[6]) # 71
    irows=np.asarray(fit1.get_ims(start_row, start_col, rows, cols, col_len, row_inc=1, col_inc=1))
    irows_index = np.arange(irows.size)
    
   
    #reshaped_irows=np.reshape(irows,(x_end-x_start,y_end-y_start))
    reshaped_irows=np.reshape(irows,(rows,cols))
    print("--------------------  Information about images  --------------------")
    print("(rank=%d) Number of images: %d " % (rank,len(irows)))
    print("--------------------   Images    --------------------")
    print(irows)
    print("--------------------    Matrix  --------------------")
    print(reshaped_irows)
    
    chi_index=ast.literal_eval(sys.argv[7]) # [0,1]
    
    print(chi_index)
    print(type(chi_index))
    
    irows_dist = [(None,None)]*num_procs
    for i in range(num_procs):
        irows_dist[i] = (irows[np.arange(i,irows.size,num_procs)],irows_index[np.arange(i,irows.size,num_procs)])
else:
    source = None
    output_path = None
    q_array = None
    integration_array = None
    sigma_array = None
    azimuth_array = None
    chi_index = None
    irows_dist = None
    
# common structures
source = comm.bcast(source, root=0)
output_path = comm.bcast(output_path, root=0)
q_array = comm.bcast(q_array, root=0)
integration_array = comm.bcast(integration_array, root=0)
sigma_array = comm.bcast(sigma_array, root=0)
azimuth_array = comm.bcast(azimuth_array, root=0)
chi_index = comm.bcast(chi_index, root=0)

# distributed list of images
irows_dist = comm.scatter(irows_dist, root=0)
irows = irows_dist[0]
irows_index = irows_dist[1]

# --- Fit1DPilatusData instance (working instance) ---
fit1 = Fit1DPilatusData(source,output_path,q_array,integration_array,sigma_array,azimuth_array)

# --- fitting ---
output = fit1.multi_fitting(irows,chi_index)

print('Check output')
print(np.shape(output))


data_coll = comm.gather([irows_dist,output], root=0)

if rank == 0:
    irows_coll = [None]*num_procs
    irows_index_coll = [None]*num_procs
    output_coll = [None]*num_procs
    nrows = 0
    for i in range(num_procs):
        irows_coll[i] = data_coll[i][0][0]
        irows_index_coll[i] = data_coll[i][0][1]
        output_coll[i] = data_coll[i][1]
        nrows += irows_coll[i].size
    # data_coll shape
    _sh_p  = output_coll[0][0].shape
    _sh_ep = output_coll[0][1].shape
    # preallocation
    p = np.zeros((_sh_p[0],nrows,)+_sh_p[2:], dtype=np.float)
    ep = np.zeros((_sh_ep[0],nrows,)+_sh_ep[2:], dtype=np.float)
    for i in range(num_procs):
        irows_index = irows_index_coll[i]
        p[:,irows_index,:] = output_coll[i][0]
        ep[:,irows_index,:] = output_coll[i][1]
    print("(rank=%d) DONE" % (rank,))
    print(p.shape)
    
    #output_name= os.path.join(
    #        output_path, "{}_{}_fit_{}_{}_{}_{}_{}".format(datetime.datetime.fromtimestamp(time()).isoformat().replace(":", "_"),
    #        sample_file,x_start,x_end,y_start,y_end, x_len)
    #    )
    #
    #np.save(output_name+'_p', p)
    #np.save(output_name+'_ep', ep)
    
    
    print('Check output')
    print('p has shape: ', np.shape(p))
    print('p has type: ', type(p))
    print('ep has shape: ', np.shape(ep))
    print('ep has type: ', type(ep))
    
    #Rank 0 saves the data as well
    print("(rank=%d) Saving fitted data" % (rank,))
    
    #Prepare sample_file
    a,b=os.path.split(sample_file)
    sample_file=b  #We through other stuff away
    
    #fit1.save_fit_output(p,ep,sample_file,output_path, irows=irows,chi_index=chi_index,x_start=x_start, x_end=x_end, y_start=y_start, y_end=y_end, x_len=x_len)
    fit1.save_fit_output(p,ep,sample_file,output_path, irows=irows,chi_index=chi_index,start_row=start_row, start_col=start_col, rows=rows, cols=cols, col_len=col_len, row_inc=1, col_inc=1)

    
    print("Finished with the fitting!")
    
