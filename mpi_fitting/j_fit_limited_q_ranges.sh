#!/bin/bash
#
# job time, change for what your job requires
#SBATCH -t 1-00:00:00
#
# job name
#SBATCH -J fit
#
#SBATCH -N 20 --exclusive
#SBATCH --tasks-per-node=48
#SBATCH --mem=90GB
#
#
# filenames stdout and stderr - customise, include %j
#SBATCH -o res_fit_%j.out
#SBATCH -e res_fit_%j.err

export OMPI_MCA_mpi_warn_on_fork=0

set -o nounset

# write this script to stdout-file - useful for scripting errors
cat $0

# load the modules required for you program - customise for your program
module purge
module add foss/2019a h5py/2.9.0 matplotlib/3.0.3-Python-3.7.2

# get arguments
#filename=$1
#x_start=$2
#x_end=$3
#y_start=$4
#y_end=$5
#x_len=$6
#chi_index=$7


filename=$1
start_row=$2
start_col=$3
rows=$4
cols=$5
col_len=$6
chi_index=$7
q_min=$8
q_max=$9



# run the program
# customise for your program name and add arguments if required
# mpirun --map-by node -n $(($SLURM_JOB_NUM_NODES*4)) python /mxn/home/gudlot/jupyter_notebooks/Nanoindenter/mpi_fitting/mpi_fit.py $filename $x_start $x_end $y_start $y_end $x_len $chi_index 
mpirun --map-by node -n $(($SLURM_JOB_NUM_NODES*4)) python /mxn/home/gudlot/jupyter_notebooks/Nanoindenter/mpi_fitting/mpi_fit_limited_q_ranges.py $filename $start_row $start_col $rows $cols $col_len $chi_index $q_min $q_max



# Start as:
# sbatch -N 10 j_fit.sh 2019-09-20T05_09_30.130000_scan_0000_pil1m_0000_integ_step.h5 47 51 47 51 71 [0,1]

#x_start=17
#x_end=19
#y_start=10
#y_end=15
#x_len=71


start_row=18
start_col=5
rows=12
cols=10
col_len=71
q_min=2.0
q_max=3.5


##sbatch -N 4 j_fit.sh TiAlN_as_deposit/2019-09-22T04_49_13.377946_scan_0007_pil1m_0000_integ_step.h5 17 19 10 12 71 [0,-1]

##sbatch -N 2 j_fit.sh TiAlN_as_deposit/2019-09-22T04_49_13.377946_scan_0007_pil1m_0000_integ_step.h5 17 20 10 13 71 [0,-1]


## sbatch -N 4 j_fit.sh TiAlN_as_deposit/2019-09-22T04_49_13.377946_scan_0007_pil1m_0000_integ_step.h5 18 5 12 10 71 [0,-1]

##sbatch -N 4 j_fit.sh TiAlN_as_deposit/2019-09-22T04_49_13.377946_scan_0007_pil1m_0000_integ_step.h5 18 5 12 10 71 [0,-1]

#New fit with get_ims
# full q range fit 3x3 images, chi_index=-1
##sbatch -N 2 j_fit.sh TiAlN_as_deposit/2019-09-22T04_49_13.377946_scan_0007_pil1m_0000_integ_step.h5 18 5 3 3 71 [-1]

# full q range fit , chi_index=-1
##sbatch -N 4 j_fit.sh TiAlN_as_deposit/2019-09-22T04_49_13.377946_scan_0007_pil1m_0000_integ_step.h5 18 5 12 10 71 [-1]


#now with limited q only
## sbatch -N 2 j_fit_limited_q_ranges.sh TiAlN_as_deposit/2019-09-22T04_49_13.377946_scan_0007_pil1m_0000_integ_step.h5 18 5 3 3 71 [-1] 2.0 3.5



