import os

class Fit1DParams:
    def __init__(self, data_input_file, output_path):
        """
        Define fitting parameters for each sample. Sample is specified via the data_input_file
        
        data_input_file: Complete path to sample
        output_path: Complete path to output folder
        
        p0: List that stores the parameter for each peak.
            Per entry: amplitude, peak position, peak width, and coefficient to determine Gaussian or Lorentzian shape [0,1]
            
        l0: List that determines which parameter of each peak in p is fitted.
            0: off
            1: on
            Last entry in this list represents the background. 
            
        """
        
        # Store locals
        self.data_input_file = data_input_file
        # set output_path and remove trailing backslash
        self.output_path = os.path.normpath(output_path)
        
         # Separate file path from input file name
        self.filepath_input_file, self.filename = os.path.split(data_input_file)
        
    def params_set(self):   
        
        # l0 initial parameters
        l0 = [[ 0, 0, 0, 0], # 2.506  1 1 1 0 small   *111  TiAlN     0   #changed from 1 0 0 0, last: 1 1 1 1
              [ 1, 1, 1, 1], # 2.669  1 1 1 0 diffuse  111  TiAlN     1
              [ 1, 1, 1, 1], # 2.669  1 1 1 1 strong   111  TiAlN     2
              [ 1, 1, 1, 1], # 3.076  1 1 1 1 strong   200  TiAlN     3
              [ 1, 1, 1, 1], # 3.345  1 1 1 1 small   *     TiAlN     4   1,0,0,0
              [ 1, 1, 1, 1], # 4.354  1 1 1 1 strong   220  TiAlN     5
              [ 1, 1, 1, 1], # 5.116  1 1 1 1 strong   311  TiAlN     6
              [ 1, 1, 1, 1], # 5.345  1 1 1 1 strong   222  TiAlN     7
              [ 1, 1, 1, 1], # 2.561  1 1 1 1 strong   111  TiN       8
              [ 1, 1, 1, 1], # 2.957  1 1 1 1 strong   200  TiN       9
              [ 1, 1, 1, 1], # 4.18   1 1 1 1          220  TiN (or a deep sub?) 10
              [ 1, 1, 1, 1], # 4.90   1 1 1 1          331  TiN       11
              [ 1, 1, 1, 1], # 5.915  1 1 1 1          400  TiN       12
              [ 1, 1, 0, 0], # deep sub                               13
              [ 1, 1, 0, 0], # deep sub                               14
              [ 1, 1, 1, 1], #100 h-AlN                               15
              [ 1, 1, 1, 1], #002 h-AlN                               16
              [ 1, 1, 1, 1], #101 h-AlN                               17
              [ 1, 1, 1, 1], #102 h-AlN                               18
              [ 1, 1, 1, 1], #110 h-AlN                               19
              [ 1, 1, 1, 1], #103 h-AlN                               20
              [ 1, 1, 1, 1], #200 h-AlN                               21
              [ 1, 1, 1, 1], #112 h-AlN                               22
              [ 1, 1, 1, 1], #201 h-AlN                               23
              [ 1, 1, 1, 1], #004 h-AlN                               24
              [ 1, 1, 1, 1], #202 h-AlN                               25
              [ 1, 1, 1, 1], #104 h-AlN                               26
              [ 1, 1, 1, 1], #203 h-AlN                               27
              [ 1, 1, 1, 1], #210 h-AlN                               28
              [ 1,1,1,1,1]] # background

        p0 = [[0.0, 2.506, 0.02, 0.0], # small *111                0   [0.15, 2.506, 0.02, 0.0]
              [9, 2.669, 0.8, 0.9],   # diffuse 111                 1
              [6, 2.669, 0.014, 0.3], # strong 111                  2
              [2, 3.076, 0.030, 0.8], # strong 200                  3
              [0.02, 3.345, 0.017, 0.0], # small *                     4
              [1.5, 4.363, 0.035, 0.3], # strong 220                  5
              [0.6, 5.116, 0.040, 0.4], # strong 311                  6
              [0.5, 5.345, 0.040, 0.4], # strong 222                  7
              [0.2, 2.561, 0.01769, 0.183], # strong 111 TiN        8
              [5.4896, 2.957, 0.029269, 0.740], # strong 200 TiN      9
              [0.05, 4.18, 0.029269, 0.2], # TiN 220                 10
              [1.0, 4.90, 0.029269, 0.5], # TiN 311                 11 
              [2.0, 5.915, 0.029269, 0.5], # TiN 400                12  
              [1.0, 1.000, 0.029269, 0.5], # deep sub               13
              [1.0, 1.7, 0.029269, 0.5], # deep sub                 14
              [1.0, 2.328053, 0.029269, 0.5], #100 h-AlN            15
              [0.05, 2.51856, 0.029269, 0.5], #002 h-AlN            16
              [1.0, 2.644218, 0.029269, 0.5], #101 h-AlN            17
              [0.3, 3.433596, 0.029269, 0.5], #102 h-AlN            18 
              [0.03, 4.05, 0.029269, 0.2], #110 h-AlN               19
              [1.0, 4.440065, 0.029269, 0.5], #103 h-AlN            20 
              [1.0, 4.651398, 0.029269, 0.5], #200 h-AlN            21 
              [1.0, 4.753759, 0.029269, 0.5], #112 h-AlN            22  
              [1.0, 4.821187, 0.029269, 0.5], #201 h-AlN            23 
              [1.0, 5.053125, 0.029269, 0.5], #004 h-AlN            24 
              [1.0, 5.310345, 0.029269, 0.5], #202 h-AlN            25 
              [1.0, 5.567922, 0.029269, 0.5], #104 h-AlN            26       
              [1.0, 6.012431, 0.029269, 0.5], #203 h-AlN            27 
              [1.0, 6.178472, 0.029269, 0.5], #210 h-AlN            28
             ] 

        # State 22.9.19
        # default values:
        # def fitting(self,irow,ichi,ps1=False, tialn_ind=[], ps2=True, tin_ind=None, ps3=True, aln_ind=None,ps4=True, bkg_ho=True, do_fit=True, do_show=True):
        # for ps sets:
        #        if value is set to True, chosen peaks are switched off
        #        if values is set to False, all peaks are used
        params_dicts = [
            #TiAlN                            #TiN          #h-AlN                                                               #deep sub      #bkh_ho           #execute fit     #show result as graph
          #{"ps1": False, "tialn_ind" : [],   "ps2" : True, "tin_ind":[8,9,12], "ps3" : True, "aln_ind" : [15,16,17,18,20]+[x for x in range(21,29)],"ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, background   
          #{"ps1": True,  "tialn_ind" : [0],  "ps2" : True, "tin_ind":[9,10,11,12], "ps3" : True, "aln_ind" : [15,16,17,18,20]+[x for x in range(21,29)],   "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, hAlN, background 
          {"ps1": False, "tialn_ind" : [],   "ps2" : False, "ps3" : True, "aln_ind" : [15,17,18,20]+[x for x in range(21,29)],"ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, background   
          {"ps1": False, "tialn_ind" : [],   "ps2" : False, "ps3" : True, "aln_ind" : [15,17,18,20]+[x for x in range(21,29)],   "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, hAlN, background 
          {"ps1": False,  "tialn_ind" : [],  "ps2" : False, "ps3" : True, "aln_ind" : [15,17,18,20]+[x for x in range(21,29)],   "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, hAlN, background 
          {"ps1": True,  "tialn_ind" : [3],  "ps2" : True,  "ps3" : True, "aln_ind" : [15,17,18,20]+[x for x in range(21,29)],   "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, but not ind=3, background   
          {"ps1": False,  "tialn_ind" : [],  "ps2" : False, "ps3" : False, "aln_ind" :[15,17,18,20]+[x for x in range(21,29)], "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, but not ind=0 TiAlNhAlN, background 
          {"ps1": False,  "tialn_ind" : [],  "ps2" : True,  "ps3" : True, "aln_ind": [15,17,20]+[x for x in range(21,29)], "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, but not ind=0, background 
          {"ps1": True,  "tialn_ind" : [4],  "ps2" : True,  "ps3" : True,                                                        "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, but not ind=2, background 
          {"ps1": True,  "tialn_ind" : [2],  "ps2" : True,  "ps3" : True,"aln_ind" : [15,16,17,18,20]+[x for x in range(21,29)], "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, but not ind=2, background 
          {"ps1": True,  "tialn_ind" : [7],  "ps2" : True,  "ps3" : True,                                                        "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, but not ind=2, background 
          {"ps1": False, "tialn_ind" : [],   "ps2" : True,  "ps3" : True,                                                        "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, background 
          {"ps1": False, "tialn_ind" : [],   "ps2" : False, "ps3" : True,                                                        "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, TiN, background
          {"ps1": False, "tialn_ind" : [],   "ps2" : True,  "ps3" : False,                                                       "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, h-AlN, background 
          {"ps1": False, "tialn_ind" : [],   "ps2" : False, "ps3" : False,                                                       "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiAlN, TiN, h-AlN, background
          {"ps1": True,  "tialn_ind" : None, "ps2" : False, "ps3" : True,                                                        "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit TiN, fit background
          {"ps1": True,  "tialn_ind" : None, "ps2" : True,  "ps3" : True,                                                        "ps4" : True,  "bkg_ho" : True,  "do_fit" : True, "do_show": True},  #fit only background
          {"ps1": False, "tialn_ind" : [],   "ps2" : False, "ps3" : True,                                                        "ps4" : True,  "bkg_ho" : False, "do_fit" : True, "do_show": True},  #fit TiAlN,TiN, but fit no ho-background
          {"ps1": False, "tialn_ind" : [],   "ps2" : True,  "ps3" : True,                                                        "ps4" : False, "bkg_ho" : True,  "do_fit" : True, "do_show": True}   #fit TiAlN and deep sub, plus background
        ]
          
            
       
          #     #TiAlN                      #TiN         h-AlN     #deep sub  #Bkg_ho       #execute fit
          #    partial(self.fitting, ps1=False, tialn_ind=[],    ps2=False,   ps3=True, aln_ind=[16,17,18,19,20]+[x for x in range(21,29)],  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, background   
          #    partial(self.fitting, ps1=False, tialn_ind=[],    ps2=False,   ps3=True, aln_ind=[16,17,18,20]+[x for x in range(21,29)],  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, background   
          #    partial(self.fitting, ps1=True,  tialn_ind=[0],   ps2=False,   ps3=True, aln_ind=[16,17,18,19]+[x for x in range(21,29)],  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, hAlN, background 
          #    partial(self.fitting, ps1=True,  tialn_ind=[3],   ps2=True,    ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=3, background   
          #    partial(self.fitting, ps1=True,  tialn_ind=[0],   ps2=False,   ps3=False, aln_ind=[],  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=0 TiAlNhAlN, background 
          #    partial(self.fitting, ps1=False, tialn_ind=[],    ps2=True,    ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, background 
          #    partial(self.fitting, ps1=True,  tialn_ind=[0],   ps2=True,    ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=0, background 
          #    partial(self.fitting, ps1=True,  tialn_ind=[2],   ps2=True,    ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=2, background 
          #    partial(self.fitting, ps1=True,  tialn_ind=[4],   ps2=True,    ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=2, background 
          #    partial(self.fitting, ps1=True,  tialn_ind=[7],   ps2=True,    ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=2, background 
          #    partial(self.fitting, ps1=False, tialn_ind=[],    ps2=False,   ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, TiN, background
          #    partial(self.fitting, ps1=False, tialn_ind=[],    ps2=True,    ps3=False, ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, h-AlN, background 
          #    partial(self.fitting, ps1=False, tialn_ind=[],    ps2=False,   ps3=False, ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, TiN, h-AlN, background
          #    partial(self.fitting, ps1=True,  tialn_ind=None,  ps2=False,   ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiN, fit background
          #    partial(self.fitting, ps1=True,  tialn_ind=None,  ps2=True,    ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit only background
          #    partial(self.fitting, ps1=False, tialn_ind=[],    ps2=False,   ps3=True,  ps4=True,  bkg_ho=False, do_fit=True, do_show=True),  #fit TiAlN,TiN, but fit no ho-background
          #    partial(self.fitting, ps1=False, tialn_ind=[],    ps2=True,    ps3=True,  ps4=False, bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN and deep sub, plus background
        
                
        return p0,l0,params_dicts