import os, copy
from functools import partial
import numpy as np
import h5py

import matplotlib.pyplot as plt
import matplotlib.ticker as plticker

#from scipy.optimize import leastsq # Levenberg-Marquadt Algorithm
from scipy.optimize import curve_fit

# Class definition
class Read1DPilatusData:
    def __init__(self, data_input_file, output_path):
        
        # Store locals
        self.data_input_file = data_input_file
        # set output_path and remove trailing backslash
        self.output_path = os.path.normpath(output_path)
        
         # Separate file path from input file name
        self.filepath_input_file, self.filename = os.path.split(data_input_file)
        
         # Open the file and store number of data entries
        with h5py.File(self.data_input_file, 'r') as hf:
            self.data_keys = list(hf.keys())
            self.total_number_entries = len(self.data_keys)
        # Check that data file exist
        with h5py.File(self.data_input_file, 'r'):
            pass

    def data_read(self):
        
        with h5py.File(self.data_input_file,'r') as h5f:
            q_array = h5f["/entry/integ/q"][()]
            integration_array = h5f["/entry/integ/data"][()]
            sigma_array = h5f["/entry/integ/sigma"][()]
            azimuth_array =h5f[ "/entry/integ/azimuth_range"][()]
        
        return q_array, integration_array, sigma_array,azimuth_array 
    

class Fit1DPilatusData:
    def __init__(self, data_input_file, output_path,q_array,integration_array,sigma_array,azimuth_array):
        
        # Store locals
        self.data_input_file = data_input_file
        # set output_path and remove trailing backslash
        self.output_path = os.path.normpath(output_path)
        
        # Define fit parameters
        self.nbkg_params = 5 # number of bkg parameters
        
        # Create the data set
        #reader = Read1DPilatusData(self.data_input_file, self.output_path)
        #[q_array, integration_array, sigma_array,azimuth_array] = reader.data_read()
        self.q_array=q_array
        self.integration_array = integration_array
        self.sigma_array=sigma_array
        self.azimuth_array=azimuth_array
        
        # Verify the number of chis
        if (np.array_equal(self.q_array.shape[-1],self.integration_array.shape[-1]) and 
            np.array_equal(self.sigma_array.shape[-1],self.integration_array.shape[-1]) and 
            np.array_equal(self.sigma_array.shape[-1],self.azimuth_array.shape[-1])) is True:
            self.nchi= q_array.shape[-1]
            
    
    # model functions for diffraction line fitting
    def gaussian(self,x,p):
        y = np.log(2.) * ( (x - p[1])/(0.5*p[2]) )**2
        y = p[0]*np.exp(-y)
        y =  y * 2.*np.sqrt(np.log(2.)/np.pi)/p[2]
        return y

    def lorentzian(self,x,p):
        y = (x - p[1])/(0.5*p[2])
        y = p[0]/(1.+y**2)
        y =  y * 2./np.pi/p[2]
        return y

    # (multi-peak) pseudoVoigth function
    # parameters = [integI, position, fwhm, LorentzianContent]
    # one raw of output matrix per parameters line (one peak in each raw)
    # you can sum them by numpy.sum(pseudoVoigt(x,p),0)
    def pseudoVoigt(self,x,p):
        a = np.array(p)
        m = int(a.size / 4)
        a = a.reshape(m,4)
        y = np.zeros((m,len(x)),dtype=np.float)
        for k in range(0,m):
            if np.absolute(a[k,0])>0.:
                y[k] = (1.-a[k,3])*self.gaussian(x,a[k,0:3])+a[k,3]*self.lorentzian(x,a[k,0:3])
        return y

    
    # model - multiple pseudovoigt with quadratic background
    def model1(self,x,*p):
        y = np.sum(self.pseudoVoigt(x,p[0:-self.nbkg_params]),0)
        y = y + p[-5]/x + p[-4]*x**3 + p[-3]*x**2 + p[-2]*x + p[-1]
        return y

    def full_param_set(self,p,p0,l0):
        l0 = [item for sublist in l0 for item in sublist]
        lindp = np.array(l0)==1 # logical index
        pp = p0.copy()
        pp[lindp] = np.array(p)[()]
        return pp

    def curve_fit_wrap(self,model,x,y,p0,l0,sigma,absolute_sigma,bounds,maxfev,method='trf'):
        def _model(x,*p):
            pp = p0.copy()
            pp[lindp] = np.array(p)[()]
            y = model(x,*pp)
            return y
        l0 = [item for sublist in l0 for item in sublist]
        lindp = np.array(l0)==1 # logical index
        pp = p0[lindp] # eliminate parameters that we do not want to fit
        bounds = (np.asarray(bounds[0])[lindp].tolist(),np.asarray(bounds[1])[lindp].tolist())
        p1, pcov = curve_fit(_model,x,y,pp,sigma=sigma,absolute_sigma=absolute_sigma,bounds=bounds,maxfev=maxfev,method=method)
        return p1, pcov

    def build_bounds(self,p,qpos=np.inf,qrwidth=np.inf):
        p_min = []
        p_max = []
        # peak bounds
        for i in range(p.size-self.nbkg_params):
            pn = i % 4
            if (pn==0):
                # intensity
                p_min = p_min + [0.]
                p_max = p_max + [np.inf]
            elif (pn==1):
                # position
                p_min = p_min + [p[i]-qpos/2]
                p_max = p_max + [p[i]+qpos/2]
            elif (pn==2):
                # width
                p_min = p_min + [0.]
                p_max = p_max + [p[i]*qrwidth]
            elif (pn==3):
                # shape
                p_min = p_min + [0.]
                p_max = p_max + [1.]
            else:
                pass
        # background bounds
        # 1/x
        p_min = p_min + [0.]
        p_max = p_max + [np.inf]
        # polynomial
        for i in range(self.nbkg_params-1):
            p_min = p_min + [-np.inf]
            p_max = p_max + [np.inf]
        return (p_min,p_max)        
    
    # fitting
    def select_good_data(self,x,y):
        lidx = y>0
        #lidx = np.logical_and(lidx, np.logical_and(x>=2.4 , x<=2.8))
        #lidx = np.logical_and(lidx, np.logical_and(x>=4.0 , x<=5.0))
        # first good
        idx1 = np.where(lidx)[0][0]
        # last good
        idx2 = np.where(lidx)[0][-1]
        lidx = np.logical_and(lidx, np.logical_and(x>=x[idx1] , x<=x[idx2]))
        
        return x[lidx],y[lidx]
    
    
    

    def fitting(self,irow,ichi,ps1=False, tialn_ind=[], ps2=True, tin_ind=None, ps3=True, aln_ind=None,ps4=True, bkg_ho=True, do_fit=True, do_show=True):

        """ 
        Peak fitting routine
        
        Parameters:
        
        irow: image/frame number
        ichi: number for a chi-range
        ps1: peak set 1, here for TiAlN
        tialn_ind: choose specif TiAlN peaks to knock out
        ps2: peak set 2, here for TiN
        ps3: peak set 3, here for h-AlN
        ps4: peak set 4, here for deep substrate
        bkg_ho: higher order background
        do_fit: execute fitting
        
        Return:
        
        peak_p: Peak parameters
        peak_ep: Errors of peak parameters
        
        Usage: 
        If parameter is set to false: procedure is not executed.
        If parameter is set to true: procedure is executed.
                
        """
        
        
    # execute the fitting process
        #image number given as irow
        #irow=100  
        #azi_index, chi_index given as ichi
        #ichi=0
        
        # get chi range
        chi_range=self.azimuth_array[irow,:,ichi]
        
        # Select data
        xx, yy= self.select_good_data(self.q_array[irow,:,ichi],self.integration_array[irow,:,ichi])
        
        # Select the errors
        # Previous assumption, poissonian noise
        ss = np.sqrt(yy)
        # Using pyFAIs error model 'poisson', see above
        # Currently not implemented, I choose np.sqrt(intensity)
        
        # estimate background
        y0 = np.mean(yy[0:5])
        y1 = np.mean(yy[-5:])
        b1 = (y1-y0)/(xx[-1]-xx[0]) # slope
        b0 = y0 - b1*xx[0] # offset
        # initial parameters
        l0 = [[ 1, 1, 1, 1], # 2.506  1 1 1 0 small   *111  TiAlN     0   #changed from 1 0 0 0
              [ 1, 1, 1, 1], # 2.669  1 1 1 0 diffuse  111  TiAlN     1
              [ 1, 1, 1, 1], # 2.669  1 1 1 1 strong   111  TiAlN     2
              [ 1, 1, 1, 1], # 3.076  1 1 1 1 strong   200  TiAlN     3
              [ 1, 0, 0, 0], # 3.345  1 1 1 1 small   *     TiAlN     4
              [ 1, 1, 1, 1], # 4.354  1 1 1 1 strong   220  TiAlN     5
              [ 1, 1, 1, 1], # 5.116  1 1 1 1 strong   311  TiAlN     6
              [ 1, 1, 1, 1], # 5.345  1 1 1 1 strong   222  TiAlN     7
              [ 1, 1, 1, 1], # 2.561  1 1 1 1 strong   111  TiN       8
              [ 1, 1, 1, 1], # 2.957  1 1 1 1 strong   200  TiN       9
              [ 1, 1, 1, 1], # 4.18   1 1 1 1          220  TiN (or a deep sub?) 10
              [ 1, 1, 1, 1], # 4.90   1 1 1 1          331  TiN       11
              [ 1, 1, 1, 1], # 5.915  1 1 1 1          400  TiN       12
              [ 1, 1, 0, 0], # deep sub                               13
              [ 1, 1, 0, 0], # deep sub                               14
              [ 1, 1, 1, 1], #100 h-AlN                               15
              [ 1, 1, 1, 1], #002 h-AlN                               16
              [ 1, 1, 1, 1], #101 h-AlN                               17
              [ 1, 1, 1, 1], #102 h-AlN                               18
              [ 1, 1, 1, 1], #110 h-AlN                               19
              [ 1, 1, 1, 1], #103 h-AlN                               20
              [ 1, 1, 1, 1], #200 h-AlN                               21
              [ 1, 1, 1, 1], #112 h-AlN                               22
              [ 1, 1, 1, 1], #201 h-AlN                               23
              [ 1, 1, 1, 1], #004 h-AlN                               24
              [ 1, 1, 1, 1], #202 h-AlN                               25
              [ 1, 1, 1, 1], #104 h-AlN                               26
              [ 1, 1, 1, 1], #203 h-AlN                               27
              [ 1, 1, 1, 1], #210 h-AlN                               28
              [ 1,1,1,1,1]] # backgrounfrom multiprocessing import Pool

#def f(x):
#    return x*x
#
#if __name__ == '__main__':
#    p = Pool(5)
#    print(p.map(f, [1, 2, 3]))
        
        if(tialn_ind in [None,[]]):
            tialn_ind = [0,1,2,3,4,5,6,7]
        if(aln_ind in [None,[]]):
            aln_ind = [15,16,17,18,19,20,21,22,23,24,25,26,27,28]
        if (tin_ind in [None,[]]):
            tin_ind = [8,9,10,11,12]    
        

        p0 = [[0.2, 2.506, 0.02, 0.0], # small *111                   0
              [5.1, 2.669, 0.7, 1.0],   # diffuse 111                 1
              [5.1, 2.669, 0.014, 0.3], # strong 111                  2
              [4.3, 3.076, 0.030, 0.8], # strong 200                  3
              [0.1, 3.345, 0.017, 0.0], # small *                     4
              [1.8, 4.363, 0.035, 0.4], # strong 220                  5
              [0.6, 5.116, 0.040, 0.4], # strong 311                  6
              [0.5, 5.345, 0.040, 0.4], # strong 222                  7
              [2.161, 2.561, 0.01769, 0.183], # strong 111 TiN        8
              [5.4896, 2.957, 0.029269, 0.740], # strong 200 TiN      9
              [1.0, 4.18, 0.029269, 0.740], # TiN 220                 10
              [1.0, 4.90, 0.029269, 0.740], # TiN 311                 11 
              [2.0, 5.915, 0.029269, 0.740], # TiN 400                12  
              [1.0, 1.000, 0.029269, 0.740], # deep sub               13
              [1.0, 1.7, 0.029269, 0.740], # deep sub                 14
              [1.0, 2.328053, 0.029269, 0.740], #100 h-AlN            15
              [1.0, 2.51856, 0.029269, 0.740], #002 h-AlN             16
              [1.0, 2.644218, 0.029269, 0.740], #101 h-AlN            17
              [1.0, 3.433596, 0.029269, 0.740], #102 h-AlN            18 
              [1.0, 4.030843, 0.029269, 0.740], #110 h-AlN            19
              [1.0, 4.440065, 0.029269, 0.740], #103 h-AlN            20 
              [1.0, 4.651398, 0.029269, 0.740], #200 h-AlN            21 
              [1.0, 4.753759, 0.029269, 0.740], #112 h-AlN            22  
              [1.0, 4.821187, 0.029269, 0.740], #201 h-AlN            23 
              [1.0, 5.053125, 0.029269, 0.740], #004 h-AlN            24 
              [1.0, 5.310345, 0.029269, 0.740], #202 h-AlN            25 
              [1.0, 5.567922, 0.029269, 0.740], #104 h-AlN            26       
              [1.0, 6.012431, 0.029269, 0.740], #203 h-AlN            27 
              [1.0, 6.178472, 0.029269, 0.740], #210 h-AlN            28
             ] 
        
        
        # Some Co peaks, only peak positions are here correct.
        # [1.0, 3.067304, 0.029269, 0.740], #111 Co
        # [1.0, 3.541818, 0.029269, 0.740], #200 Co
        # [1.0, 5.008887, 0.029269, 0.740], #220 Co
        # [1.0, 5.873441, 0.029269, 0.740], #311 Co  
        
        
        #Potential peaks:
        #	peak	Q
        #WC	001	2.213469672
        #	100	2.494987346
        #	101	3.335023986#
        #	110	4.322084816
        #	002	4.427578259
        #	111	4.855742347
        #		
        #Co	111	3.061531074
        #	200	3.531690495
        #	220	5.002511151
        
        
        #h-AlN hkl Q (A-1)
        #      100 2.32982621
        #      002 2.52377231
        #      101 2.65344147
        #      102 3.43488723
        #      110 4.02576767
        #      103 4.44466219

        
        # Number of peaks (no background)
        npeaks = np.array(p0).shape[0]
        
        # set zero intensity and fix some TiAlN peaks
        #tialn_ind=[2]
        #tialn_ind=[7]
        #tialn_ind=[4]
        #tialn_ind=[2]
        #if (True):
        if ps1:
            print('\033[35mWarning! Some TiAlN peaks are fixed. Peak(s) fixed: %s\033[0m' %(tialn_ind))
            for i in tialn_ind:
                for k in range(4):
                    l0[i][k] = 0 # fix
                    p0[i][0] = 0 # set zero intensity
                    #print(l0[i],p0[i])
       
        # set zero intensity and fix tin peaks
        #if(True):
        if ps2:
            print('\033[35mWarning! Some TiN peaks are fixed.\033[0m')
            for i in tin_ind:
                for k in range(4):
                    l0[i][k] = 0 # fix
                    p0[i][0] = 0 # set zero intensity
        
        # set zero intensity and fix h-AlN peaks
        #if(True):
        if ps3:
            print('\033[35mWarning! Some h-Aln peaks are fixed.\033[0m')
            for i in aln_ind:
                for k in range(4):
                    l0[i][k] = 0 # fix
                    p0[i][0] = 0 # set zero intensity
        
        
        # set zero intensity and fix deep-sub peaks
        #if(True):
        if ps4:
            print('\033[34mWarning! Deep substrate peaks are fixed.\033[0m')
            for i in deep_sub_ind:
                for k in range(4):
                    l0[i][k] = 0 # fix
                    p0[i][0] = 0 # set zero intensity           
                    

        # fix all peaks (do not use !) (False = disabled)
        if(False):
            print('\033[31mWarning! Peaks are all fixed.\033[0m')
            for k in range(npeaks):
                l0[k][1] = 0 # position
                l0[k][2] = 0 # width
                l0[k][3] = 0 # shape

        p0 = np.concatenate((np.array(p0).reshape(npeaks*4),[0.0,0.0,0.0,b1,b0]))
        perr = copy.deepcopy(p0)*0
        p1 = copy.deepcopy(p0)*0
        bounds = self.build_bounds(p0,qpos=0.05,qrwidth=2.0)
        
        # fitting
        #if (True):
        if do_fit:
            # --- the 1st fit stage ---
            _l0 = copy.deepcopy(l0)
            
            # fix higher order bkg coeff
            # Can be switched off and on now
            #if (True):
            if bkg_ho:
                if bkg_ho is False:
                    print('\033[33mWarning! Higher order background is fixed.\033[35m')
                    _l0[-1][-(self.nbkg_params-1):-2] = [0]*(self.nbkg_params-3)

            # fix all peak width and shape parameter and all positions
            for k in range(npeaks):
                _l0[k][1] = 0 # position
                _l0[k][2] = 0 # width
                _l0[k][3] = 0 # shape
            p1, pcov = self.curve_fit_wrap(self.model1,xx,yy,p0,_l0,sigma=ss,absolute_sigma=True,bounds=bounds,maxfev=100,method='trf')
            # return non-fitted parameters
            p0 = self.full_param_set(p1,p0,_l0)
            # --- the 2nd fit stage ---
            _l0 = copy.deepcopy(l0)
            # fix all peak width and shape parameter and all positions
            for k in range(npeaks):
                _l0[k][1] = 0 # position
                _l0[k][2] = 0 # width
                _l0[k][3] = 0 # shape
            p1, pcov = self.curve_fit_wrap(self.model1,xx,yy,p0,_l0,sigma=ss,absolute_sigma=True,bounds=bounds,maxfev=100,method='trf')
            # return non-fitted parameters
            p0 = self.full_param_set(p1,p0,_l0)
            # --- the 3rd fit stage ---
            _l0 = copy.deepcopy(l0)
            _p0 = copy.deepcopy(p0)
            # fix all peak parameters, outside of range
            for k in range(npeaks):
                if ((_p0[k*4+1]>=xx[-1]) or (_p0[k*4+1]<=xx[0])):
                    _l0[k][0] = 0 # intensity
                    _l0[k][1] = 0 # position
                    _l0[k][2] = 0 # width
                    _l0[k][3] = 0 # shape
                    _p0[k*4+0] = 0 # set intensity to zero
            # fix all peak width and shape parameter and all positions
            for k in range(npeaks):
                _l0[k][3] = 0 # shape
            p1, pcov = self.curve_fit_wrap(self.model1,xx,yy,_p0,_l0,sigma=ss,absolute_sigma=True,bounds=bounds,maxfev=100,method='trf')
            # return non-fitted parameters
            p0 = self.full_param_set(p1,_p0,_l0)
            # --- the 4th fit stage ---
            _l0 = copy.deepcopy(l0)
            _p0 = copy.deepcopy(p0)
            # fix all peak parameters, outside of range
            for k in range(npeaks):
                if ((_p0[k*4+1]>=xx[-1]) or (_p0[k*4+1]<=xx[0])):
                    _l0[k][0] = 0 # intensity
                    _l0[k][1] = 0 # position
                    _l0[k][2] = 0 # width
                    _l0[k][3] = 0 # shape
                    _p0[k*4+0] = 0 # set intensity to zero
            p1, pcov = self.curve_fit_wrap(self.model1,xx,yy,_p0,_l0,sigma=ss,absolute_sigma=True,bounds=bounds,maxfev=500,method='trf')
            perr = np.sqrt(np.diag(pcov))
            # return non-fitted parameters
            p1 = self.full_param_set(p1,_p0,_l0)
            perr = self.full_param_set(perr,np.zeros(p1.shape),_l0)
            yyc = self.model1(xx,*p1)
        else:
            # not fitting
            p1 = copy.deepcopy(p0)
            yyc = self.model1(xx,*p0)
        
        print('\nnchi %d, chi_range (%.1f %.1f)' %(ichi, chi_range[0], chi_range[1]))
        print( np.array(p1[:-self.nbkg_params]).reshape(npeaks,4) )
        print( p1[-self.nbkg_params:] )

        peak_p = np.array(p1[:-self.nbkg_params]).reshape(npeaks,4)
        peak_ep = np.array(perr[:-self.nbkg_params]).reshape(npeaks,4) 
        
        # plot datafrom multiprocessing import Pool

#def f(x):
    #return x*x

#if __name__ == '__main__':
   # p = Pool(5)
    #print(p.map(f, [1, 2, 3]))
        
         
        if do_show:
            font1=13
            locator_value=0.5

            plt.figure(figsize=(17,5))
            ax1=plt.subplot(121)
            plt.plot(xx,yy,"*")
            plt.plot(xx,yyc,"-")
            plt.plot(xx,yyc-yy,"g-")
            plt.title('Fit, frame: %d, azimuthal range (%.1f$\degree$, %.1f$\degree$), $\chi_{center}=$%.1f$\degree$, ichi=%d' 
                      %(irow, chi_range[0], chi_range[1], (chi_range[1]-chi_range[0])/2+chi_range[0],ichi),fontdict={'fontsize': font1}) 
            plt.xlabel('q $[\AA^{-1}]$',fontdict={'fontsize':font1})
            plt.ylabel('Intensity',fontdict={'fontsize':font1})
            plt.grid(which='both',axis='both')
            loc = plticker.MultipleLocator(base=locator_value) # this locator puts ticks at regular intervals
            ax1.xaxis.set_major_locator(loc)

            ax2=plt.subplot(122)
            plt.semilogy(xx,yy,"*")
            plt.semilogy(xx,yyc,"-")

            #No peak lines
            #for i in range(0,npeaks):
            #    plt.axvline(x=peak_p[i][1],color='midnightblue',alpha=0.3)

            plt.title('Fit, frame: %d, azimuthal range (%.1f$\degree$, %.1f$\degree$), $\chi_{center}=$%.1f$\degree$, ichi=%d, do_fit=%r' 
                      %(irow, chi_range[0], chi_range[1], (chi_range[1]-chi_range[0])/2+chi_range[0],ichi, do_fit),fontdict={'fontsize': font1}) 
            plt.xlabel('q $[\AA^{-1}]$',fontdict={'fontsize':font1})
            plt.ylabel('log(Intensity)',fontdict={'fontsize':font1})
            plt.grid(which='both',axis='both')
            loc = plticker.MultipleLocator(base=locator_value) # this locator puts ticks at regular intervals
            ax2.xaxis.set_major_locator(loc)
            plt.tight_layout()
            plt.show()
        
        

        # The fitting function returns the parameter-matrix and its error-matrix.
        return peak_p, peak_ep, npeaks 
    
    def fitting_functions(self):
        
        #Below: Stand 22.9.2019, works only for TiAlN as deposited
        #fittings = (            #TiAlN                      #TiN                            #h-AlN     #deep sub  #Bkg_ho       #execute fit
        #  partial(self.fitting, ps1=False, tialn_ind=[],    ps2=False,  ps3=True, aln_ind=[16,17,18,19,20]+[x for x in range(21,29)],  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, background   
        #  partial(self.fitting, ps1=False, tialn_ind=[],    ps2=False,  ps3=True, aln_ind=[16,17,18,20]+[x for x in range(21,29)],  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, background   
        #  partial(self.fitting, ps1=True,  tialn_ind=[0],   ps2=False,  ps3=True, aln_ind=[16,17,18,19]+[x for x in range(21,29)],  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, hAlN, background 
        #  partial(self.fitting, ps1=True,  tialn_ind=[3],   ps2=True,   ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=3, background   
        #  partial(self.fitting, ps1=True,  tialn_ind=[0],   ps2=False,  ps3=False, aln_ind=[],  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=0 TiAlNhAlN, background 
        #  partial(self.fitting, ps1=False, tialn_ind=[],    ps2=True,   ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, background 
        #  partial(self.fitting, ps1=True,  tialn_ind=[0],   ps2=True,   ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=0, background 
        #  partial(self.fitting, ps1=True,  tialn_ind=[2],   ps2=True,   ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=2, background 
        #  partial(self.fitting, ps1=True,  tialn_ind=[4],   ps2=True,   ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=2, background 
        #  partial(self.fitting, ps1=True,  tialn_ind=[7],   ps2=True,   ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=2, background 
        #  partial(self.fitting, ps1=False, tialn_ind=[],    ps2=False,  ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, TiN, background
        #  partial(self.fitting, ps1=False, tialn_ind=[],    ps2=True,   ps3=False, ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, h-AlN, background 
        #  partial(self.fitting, ps1=False, tialn_ind=[],    ps2=False,  ps3=False, ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, TiN, h-AlN, background
        #  partial(self.fitting, ps1=True,  tialn_ind=None,  ps2=False,  ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiN, fit background
        #  partial(self.fitting, ps1=True,  tialn_ind=None,  ps2=True,   ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit only background
        #  partial(self.fitting, ps1=False, tialn_ind=[],    ps2=False,  ps3=True,  ps4=True,  bkg_ho=False, do_fit=True, do_show=True),  #fit TiAlN,TiN, but fit no ho-background
        #  partial(self.fitting, ps1=False, tialn_ind=[],    ps2=True,   ps3=True,  ps4=False, bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN and deep sub, plus background
        #    )
        
        
        
        
        # First veersion
        #fittings = (            #TiAlN                                     #TiN       #h-AlN     #deep sub  #Bkg_ho       #execute fit
        #  partial(self.fitting, ps1=True, tialn_ind=[0],                   ps2=True,  ps3=True, aln_ind=[16,17,18,19]+[x for x in range(21,29)],  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, hAlN, background 
        #  partial(self.fitting, ps1=False, tialn_ind=[],                   ps2=True,  ps3=False,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, hAlN, background 
        #  partial(self.fitting, ps1=False, tialn_ind=[],                   ps2=True,  ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, background 
        #  partial(self.fitting, ps1=True, tialn_ind=[0],                  ps2=True,  ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=0, background 
        #  partial(self.fitting, ps1=True, tialn_ind=[2],                  ps2=True,  ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=2, background 
        #  partial(self.fitting, ps1=True, tialn_ind=[3],               ps2=True,  ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=3, background   
        #  partial(self.fitting, ps1=True, tialn_ind=[4],                  ps2=True,  ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=2, background 
        #  partial(self.fitting, ps1=True, tialn_ind=[7],                  ps2=True,  ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, but not ind=2, background 
        #  partial(self.fitting, ps1=False, tialn_ind=[],                   ps2=False, ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, TiN, background
        #  partial(self.fitting, ps1=False, tialn_ind=[],                   ps2=True,  ps3=False, ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, h-AlN, background 
        #  partial(self.fitting, ps1=False, tialn_ind=[],                   ps2=False, ps3=False, ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN, TiN, h-AlN, background
        #  partial(self.fitting, ps1=True,  tialn_ind=None,                ps2=False, ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit TiN, fit background
        #  partial(self.fitting, ps1=True,  tialn_ind=None,               ps2=True,  ps3=True,  ps4=True,  bkg_ho=True,  do_fit=True, do_show=True),  #fit only background
       #  partial(self.fitting, ps1=False, tialn_ind=[],                   ps2=False, ps3=True,  ps4=True,  bkg_ho=False, do_fit=True, do_show=True),  #fit TiAlN,TiN, but fit no ho-background
       #   partial(self.fitting, ps1=False, tialn_ind=[],                   ps2=True,  ps3=True,  ps4=False, bkg_ho=True,  do_fit=True, do_show=True),  #fit TiAlN and deep sub, plus background
       #     )

        for fit in fittings:
            yield fit


    def get_irows(self, x_start, x_end, y_start, y_end,  x_len):
        output = []
        for i in range(y_start, y_end):
            y_offset = x_len * i
            for j in range(x_start + y_offset, x_end + y_offset):
                output.append(j)
        return output

    def multi_fitting(self, irows, chi_index=[0]):
           
        # Save the result, preparation
        nchi = self.nchi
        nimg = len(irows)
        
        # call once the fitting func to get npeaks, no fitting
        
        print("\033[33mDummy output\033[0m")
        dummy_result=self.fitting(irows[0], chi_index[0], ps1=False, tialn_ind=[],  ps2=True, ps3=True, ps4=True,  bkg_ho=True,  do_fit=False, do_show=False)
        # npeaks
        npeaks=dummy_result[2]
        
        print("\033[33m++++++++++++++++\033[0m"*3)
        
        
        # Allocate the storage matrix
        dset_p = np.zeros((nchi,nimg,npeaks,4),dtype=np.float)
        dset_p_ep = np.zeros((nchi,nimg,npeaks,4),dtype=np.float)
        print(dset_p.shape)
        print(dset_p_ep.shape)
        self._iter_irow(irows, chi_index, npeaks, dset_p, dset_p_ep)
        print("#######################################")
        print(dset_p)
        print("#######################################")
        return dset_p, dset_p_ep
        
    
    def _try_fittings(self, irow,  ichi, npeaks):
        """ Per"""
        for fit_func in self.fitting_functions():
            print("\nTrying fitting with : ", irow, ichi, fit_func)
            try:
                result = fit_func(irow, ichi)
                print("\033[92m----> Succeed to fit with: ", fit_func,  "\033[0m")
                return result , ichi
            except RuntimeError as e:
                print("\033[93m----> Fail to fit with: ", irow, ichi, e,"\n",fit_func, "\033[0m")
        return (([[np.nan, np.nan , np.nan, np.nan]]*npeaks, [[np.nan, np.nan , np.nan, np.nan]]*npeaks, npeaks)), ichi # TODO fix it


    def _iter_ichi(self, irow, irow_index, chi_index, npeaks, p_array, p_ep_array):
        for ichi in chi_index:
            print("\n#############\nICHI", ichi)
            output, _ = self._try_fittings(irow, ichi, npeaks)
            p, p_ep, peaks = output
            p_array[ichi][irow_index] = p 
            p_ep_array[ichi][irow_index] = p_ep
    
    #def _iter_ichi_multi(self, irow, irow_index, chi_index, npeaks, p_array, p_ep_array):
    #    f = partial(self._iter_ichi, irow=irow, npeaks=npeaks)
    #    output = pool.map(f, chi_index)
    #    for result, ichi in output:
    #        p, p_ep, peaks = result
    #        p_array[ichi][irow_index] = p 
    #        p_ep_array[ichi][irow_index] = p_ep
    
            
    def _iter_irow(self, irows, chi_index, nbpeaks, p_array, p_ep_array):
        for irow_index, irow in enumerate(irows):        
            print("\033[95m++++++++++++++++"*3)
            print('IROW', irow, "\033[0m")
            self._iter_ichi(irow, irow_index, chi_index, nbpeaks, p_array, p_ep_array)
            

class PeakPositions_TiAlN:
    def __init__(self, hkl_l=None, apar=None):
        
        if hkl_l is None:
            hkl_l = [[1,1,1],[2,0,0],[2,2,0],[3,1,1],[2,2,2],[4,0,0]]
            Qhkl = np.zeros((len(hkl_l),))
        if apar is None:
            apar = 4.09 # (A)
        for k in range(len(hkl_l)):
            hkl = np.asarray(hkl_l[k])
            Qhkl[k] = 2*np.pi/apar*np.sqrt(np.dot(hkl,hkl))
            print("TiAlN %d %d %d    %f" % (hkl[0],hkl[1],hkl[2],Qhkl[k]))
    
        
class PeakPositions_TiN:
    def __init__(self, hkl_l=None,apar=None):
        
        if hkl_l is None:
            hkl_l = [[1,1,1],[2,0,0],[2,2,0],[3,1,1],[2,2,2],[4,0,0]]
            Qhkl = np.zeros((len(hkl_l),))
        if apar is None:
            apar = 4.249 # (A) #Wikipedia   #Magnus: 4.2
        for k in range(len(hkl_l)):
            hkl = np.asarray(hkl_l[k])
            Qhkl[k] = 2*np.pi/apar*np.sqrt(np.dot(hkl,hkl))
            print("TiN %d %d %d    %f" % (hkl[0],hkl[1],hkl[2],Qhkl[k]))
            
class PeakPositions_Co:  
      def __init__(self, hkl_l=None,apar=None):
        
        if hkl_l is None:
            hkl_l = [[1,1,1],[2,0,0],[2,2,0],[3,1,1]]
            Qhkl = np.zeros((len(hkl_l),))
        if apar is None:
            apar = 3.548 # (A) #Wikipedia    
        for k in range(len(hkl_l)):
            hkl = np.asarray(hkl_l[k])
            Qhkl[k] = 2*np.pi/apar*np.sqrt(np.dot(hkl,hkl))
            print("Co %d %d %d    %f" % (hkl[0],hkl[1],hkl[2],Qhkl[k]))

            
if __name__ == '__main__':
    PeakPositions_TiAlN()      
    PeakPositions_TiN()
    PeakPositions_Co()
