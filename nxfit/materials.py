import numpy as np

class PeakPositions_TiAlN:
    def __init__(self, hkl_l=None, apar=None):
        
        if hkl_l is None:
            hkl_l = [[1,1,1],[2,0,0],[2,2,0],[3,1,1],[2,2,2],[4,0,0]]
            Qhkl = np.zeros((len(hkl_l),))
        if apar is None:
            apar = 4.09 # (A)
        for k in range(len(hkl_l)):
            hkl = np.asarray(hkl_l[k])
            Qhkl[k] = 2*np.pi/apar*np.sqrt(np.dot(hkl,hkl))
            print("TiAlN %d %d %d    %f" % (hkl[0],hkl[1],hkl[2],Qhkl[k]))


class PeakPositions_TiN:
    def __init__(self, hkl_l=None,apar=None):    
        if hkl_l is None:
            hkl_l = [[1,1,1],[2,0,0],[2,2,0],[3,1,1],[2,2,2],[4,0,0]]
            Qhkl = np.zeros((len(hkl_l),))
        if apar is None:
            apar = 4.249 # (A) #Wikipedia   #Magnus: 4.2
        for k in range(len(hkl_l)):
            hkl = np.asarray(hkl_l[k])
            Qhkl[k] = 2*np.pi/apar*np.sqrt(np.dot(hkl,hkl))
            print("TiN %d %d %d    %f" % (hkl[0],hkl[1],hkl[2],Qhkl[k]))


class PeakPositions_Co:  
      def __init__(self, hkl_l=None,apar=None):
        
        if hkl_l is None:
            hkl_l = [[1,1,1],[2,0,0],[2,2,0],[3,1,1]]
            Qhkl = np.zeros((len(hkl_l),))
        if apar is None:
            apar = 3.548 # (A) #Wikipedia    
        for k in range(len(hkl_l)):
            hkl = np.asarray(hkl_l[k])
            Qhkl[k] = 2*np.pi/apar*np.sqrt(np.dot(hkl,hkl))
            print("Co %d %d %d    %f" % (hkl[0],hkl[1],hkl[2],Qhkl[k]))