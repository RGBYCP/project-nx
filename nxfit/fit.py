import os, copy
from functools import partial
import numpy as np
import h5py
import datetime
from time import time

import matplotlib.pyplot as plt
import matplotlib.ticker as plticker

#from scipy.optimize import leastsq # Levenberg-Marquadt Algorithm
from scipy.optimize import curve_fit

class Fit1D:
    def __init__(self, data_input_file, output_path, q_array, integration_array, sigma_array, azimuth_array, p0, l0, params_dicts):
        
        # Store locals
        self.data_input_file = data_input_file
        # set output_path and remove trailing backslash
        self.output_path = os.path.normpath(output_path)
        
        # Define fit parameters
        self.nbkg_params = 5 # number of bkg parameters
        
        # Create the data set
        #reader = Read1DPilatusData(self.data_input_file, self.output_path)
        #[q_array, integration_array, sigma_array,azimuth_array] = reader.data_read()
        self.q_array=q_array
        self.integration_array = integration_array
        self.sigma_array=sigma_array
        self.azimuth_array=azimuth_array
        
        # Verify the number of chis
        if (np.array_equal(self.q_array.shape[-1],self.integration_array.shape[-1]) and 
            np.array_equal(self.sigma_array.shape[-1],self.integration_array.shape[-1]) and 
            np.array_equal(self.sigma_array.shape[-1],self.azimuth_array.shape[-1])) is True:
            self.nchi= q_array.shape[-1]
            
        # Verify type of p0, l0
        if not all(isinstance(i, list) for i in [p0,l0,params_dicts]):
            raise TypeError('p0, l0, params_dicts need to be of type list.')

        self.p0=p0
        self.l0=l0
        self.params_dicts=params_dicts
            
    
    # model functions for diffraction line fitting
    def gaussian(self,x,p):
        y = np.log(2.) * ( (x - p[1])/(0.5*p[2]) )**2
        y = p[0]*np.exp(-y)
        y =  y * 2.*np.sqrt(np.log(2.)/np.pi)/p[2]
        return y

    def lorentzian(self,x,p):
        y = (x - p[1])/(0.5*p[2])
        y = p[0]/(1.+y**2)
        y =  y * 2./np.pi/p[2]
        return y

    # (multi-peak) pseudoVoigt function
    # parameters = [integI, position, fwhm, LorentzianContent]
    # one raw of output matrix per parameters line (one peak in each raw)
    # you can sum them by numpy.sum(pseudoVoigt(x,p),0)
    def pseudoVoigt(self,x,p):
        a = np.array(p)
        m = int(a.size / 4)
        a = a.reshape(m,4)
        y = np.zeros((m,len(x)),dtype=np.float)
        for k in range(0,m):
            if np.absolute(a[k,0])>0.:
                y[k] = (1.-a[k,3])*self.gaussian(x,a[k,0:3])+a[k,3]*self.lorentzian(x,a[k,0:3])
        return y

    
    # model - multiple pseudovoigt with quadratic background
    def model1(self,x,*p):
        y = np.sum(self.pseudoVoigt(x,p[0:-self.nbkg_params]),0)
        y = y + p[-5]/x + p[-4]*x**3 + p[-3]*x**2 + p[-2]*x + p[-1]
        return y

    def full_param_set(self,p,p0,l0):
        l0 = [item for sublist in l0 for item in sublist]
        lindp = np.array(l0)==1 # logical index
        pp = p0.copy()
        pp[lindp] = np.array(p)[()]
        return pp

    def curve_fit_wrap(self,model,x,y,p0,l0,sigma,absolute_sigma,bounds,maxfev,method='trf'):
        def _model(x,*p):
            pp = p0.copy()
            pp[lindp] = np.array(p)[()]
            y = model(x,*pp)
            return y
        l0 = [item for sublist in l0 for item in sublist]
        lindp = np.array(l0)==1 # logical index
        pp = p0[lindp] # eliminate parameters that we do not want to fit
        bounds = (np.asarray(bounds[0])[lindp].tolist(),np.asarray(bounds[1])[lindp].tolist())
        p1, pcov = curve_fit(_model,x,y,pp,sigma=sigma,absolute_sigma=absolute_sigma,bounds=bounds,maxfev=maxfev,method=method)
        return p1, pcov

    def build_bounds(self,p,qpos=np.inf,qrwidth=np.inf):
        p_min = []
        p_max = []
        # peak bounds
        for i in range(p.size-self.nbkg_params):
            pn = i % 4
            if (pn==0):
                # intensity
                p_min = p_min + [0.]
                p_max = p_max + [np.inf]
            elif (pn==1):
                # position
                p_min = p_min + [p[i]-qpos/2]
                p_max = p_max + [p[i]+qpos/2]
            elif (pn==2):
                # width
                p_min = p_min + [0.]
                p_max = p_max + [p[i]*qrwidth]
            elif (pn==3):
                # shape
                p_min = p_min + [0.]
                p_max = p_max + [1.]
            else:
                pass
        # background bounds
        # 1/x
        p_min = p_min + [0.]
        p_max = p_max + [np.inf]
        # polynomial
        for i in range(self.nbkg_params-1):
            p_min = p_min + [-np.inf]
            p_max = p_max + [np.inf]
        return (p_min,p_max)        
    
    # fitting
    def select_good_data(self,x,y):
        lidx = y>0
        #lidx = np.logical_and(lidx, np.logical_and(x>=2.4 , x<=2.8))
        #lidx = np.logical_and(lidx, np.logical_and(x>=4.0 , x<=5.0))
        # first good
        idx1 = np.where(lidx)[0][0]
        # last good
        idx2 = np.where(lidx)[0][-1]
        lidx = np.logical_and(lidx, np.logical_and(x>=x[idx1] , x<=x[idx2]))
        
        return x[lidx],y[lidx]
    
    def fitting(self,irow,ichi,ps1=False, tialn_ind=[], ps2=True, tin_ind=None, ps3=True, aln_ind=None,ps4=True, bkg_ho=True, do_fit=True, do_show=True):
        """ 
        Peak fitting routine
        
        Parameters:
        
        irow: image/frame number
        ichi: number for a chi-range
        ps1: peak set 1, here for TiAlN
        tialn_ind: choose specif TiAlN peaks to knock out
        ps2: peak set 2, here for TiN
        ps3: peak set 3, here for h-AlN
        ps4: peak set 4, here for deep substrate
        bkg_ho: higher order background
        do_fit: execute fitting
        
        Return:
        
        peak_p: Peak parameters
        peak_ep: Errors of peak parameters
        
        Usage: 
        If parameter is set to false: procedure is not executed.
        If parameter is set to true: procedure is executed.
                
        """
        
        # Define p0,l0
        p0 = copy.deepcopy(self.p0)
        l0 = copy.deepcopy(self.l0)
        
        
        print(p0)
        print(l0)
        print("XXXXXXXXXXXXXXXXX"*4)
        
    
        # get chi range
        chi_range = self.azimuth_array[irow,:,ichi]
        
        # Select data
        xx, yy = self.select_good_data(self.q_array[irow,:,ichi],self.integration_array[irow,:,ichi])
        
        # Select the errors
        # Previous assumption, poissonian noise
        ss = np.sqrt(yy)
        # Using pyFAIs error model 'poisson', see above
        # Currently not implemented, I choose np.sqrt(intensity)
        
        # estimate background
        y0 = np.mean(yy[0:5])
        y1 = np.mean(yy[-5:])
        b1 = (y1-y0)/(xx[-1]-xx[0]) # slope
        b0 = y0 - b1*xx[0] # offset
        
        # initial parameters
        # here was list l0

        print("At begin Function aln_ind values:",aln_ind)
        
        # Assign set of indices to different materials if none are given or variable is empty
        if(tialn_ind in [None,[]]):
            tialn_ind = [0,1,2,3,4,5,6,7]
        if (tin_ind in [None,[]]):
            tin_ind = [8,9,10,11,12]    
        if(aln_ind in [None,[]]):
            aln_ind = [15,16,17,18,19,20,21,22,23,24,25,26,27,28]
        
        
        deep_sub_ind = [13,14]
       
        # here was the list for p0


        # Number of peaks (no background)
        npeaks = np.array(p0).shape[0]
        
        # set zero intensity and fix some TiAlN peaks
        if ps1:
            print('\033[35mWarning! Some TiAlN peaks are fixed. Peak(s) fixed: %s\033[0m' %(tialn_ind))
            for i in tialn_ind:
                for k in range(4):
                    l0[i][k] = 0 # fix
                    p0[i][0] = 0 # set zero intensity
                    #print(l0[i],p0[i])
       
        # set zero intensity and fix tin peaks
        if ps2:
            print('\033[35mWarning! Some TiN peaks are fixed. Peak(s) fixed: %s\033[0m' %(tin_ind))
            for i in tin_ind:
                for k in range(4):
                    l0[i][k] = 0 # fix
                    p0[i][0] = 0 # set zero intensity
                    
        
        # set zero intensity and fix h-AlN peaks
        if ps3:
            print('\033[35mWarning! Some h-Aln peaks are fixed. Peak(s) fixed: %s\033[0m' %(aln_ind))
            print("Aln_ind: ",aln_ind)
            for i in aln_ind:
                print(i)
                for k in range(4):
                    l0[i][k] = 0 # fix
                    p0[i][0] = 0 # set zero intensity
        print("p0 after peak selection:\n",p0)
        
        # set zero intensity and fix deep-sub peaks
        #if(True):
        if ps4:
            print('\033[34mWarning! Deep substrate peaks are fixed.\033[0m')
            for i in deep_sub_ind:
                for k in range(4):
                    l0[i][k] = 0 # fix
                    p0[i][0] = 0 # set zero intensity  
                    
        # fix higher order bkg coeff
        # can be switched off and on now
        if bkg_ho is False:
           
            print('\033[33mWarning! Higher order background is fixed.\033[0m')
            #Original version:
            #_l0[-1][-(self.nbkg_params-1):-2] = [0]*(self.nbkg_params-3)

            #Question for Zdenek/Gudrun's version:
            #_l0[-1][-(self.nbkg_params):-3] = [0]*(self.nbkg_params-3)

            #Suggestion to fix bkg of higher order in l0 as well.
            l0[-1][-(self.nbkg_params):-3] = [0]*(self.nbkg_params-3)
           

        # fix all peaks (do not use !) (False = disabled)
        if(False):
            print('\033[31mWarning! Peaks are all fixed.\033[0m')
            for k in range(npeaks):
                l0[k][1] = 0 # position
                l0[k][2] = 0 # width
                l0[k][3] = 0 # shape

        p0 = np.concatenate((np.array(p0).reshape(npeaks*4),[0.0,0.0,0.0,b1,b0]))
        perr = copy.deepcopy(p0)*0
        p1 = copy.deepcopy(p0)*0
        bounds = self.build_bounds(p0,qpos=0.05,qrwidth=2.0)
        
        # fitting
        if do_fit:
            # --- the 1st fit stage ---
            _l0 = copy.deepcopy(l0)
            
            # fix all peak width and shape parameter and all positions
            for k in range(npeaks):
                _l0[k][1] = 0 # position
                _l0[k][2] = 0 # width
                _l0[k][3] = 0 # shape
            p1, pcov = self.curve_fit_wrap(self.model1,xx,yy,p0,_l0,sigma=ss,absolute_sigma=True,bounds=bounds,maxfev=100,method='trf')
            # return non-fitted parameters
            p0 = self.full_param_set(p1,p0,_l0)
            
    
            
            # --- the 2nd fit stage ---
            _l0 = copy.deepcopy(l0)
            
         
            # fix all peak width and shape parameter and all positions
            for k in range(npeaks):
                _l0[k][1] = 0 # position
                _l0[k][2] = 0 # width
                _l0[k][3] = 0 # shape
            p1, pcov = self.curve_fit_wrap(self.model1,xx,yy,p0,_l0,sigma=ss,absolute_sigma=True,bounds=bounds,maxfev=100,method='trf')
            # return non-fitted parameters
            p0 = self.full_param_set(p1,p0,_l0)
            # --- the 3rd fit stage ---
            _l0 = copy.deepcopy(l0)
            _p0 = copy.deepcopy(p0)
            # fix all peak parameters, outside of range
            for k in range(npeaks):
                if ((_p0[k*4+1]>=xx[-1]) or (_p0[k*4+1]<=xx[0])):
                    _l0[k][0] = 0 # intensity
                    _l0[k][1] = 0 # position
                    _l0[k][2] = 0 # width
                    _l0[k][3] = 0 # shape
                    _p0[k*4+0] = 0 # set intensity to zero
            # fix all peak width and shape parameter and all positions
            for k in range(npeaks):
                _l0[k][3] = 0 # shape
            p1, pcov = self.curve_fit_wrap(self.model1,xx,yy,_p0,_l0,sigma=ss,absolute_sigma=True,bounds=bounds,maxfev=100,method='trf')
            # return non-fitted parameters
            p0 = self.full_param_set(p1,_p0,_l0)
            # --- the 4th fit stage ---
            _l0 = copy.deepcopy(l0)
            _p0 = copy.deepcopy(p0)
            # fix all peak parameters, outside of range
            for k in range(npeaks):
                if ((_p0[k*4+1]>=xx[-1]) or (_p0[k*4+1]<=xx[0])):
                    _l0[k][0] = 0 # intensity
                    _l0[k][1] = 0 # position
                    _l0[k][2] = 0 # width
                    _l0[k][3] = 0 # shape
                    _p0[k*4+0] = 0 # set intensity to zero
            p1, pcov = self.curve_fit_wrap(self.model1,xx,yy,_p0,_l0,sigma=ss,absolute_sigma=True,bounds=bounds,maxfev=500,method='trf')
            perr = np.sqrt(np.diag(pcov))
            # return non-fitted parameters
            p1 = self.full_param_set(p1,_p0,_l0)
            perr = self.full_param_set(perr,np.zeros(p1.shape),_l0)
            yyc = self.model1(xx,*p1)
        else:
            # not fitting
            p1 = copy.deepcopy(p0)
            yyc = self.model1(xx,*p0)
        
        print('\nnchi %d, chi_range (%.1f %.1f)' %(ichi, chi_range[0], chi_range[1]))
        print( np.array(p1[:-self.nbkg_params]).reshape(npeaks,4) )
        print( p1[-self.nbkg_params:] )

        peak_p = np.array(p1[:-self.nbkg_params]).reshape(npeaks,4)
        peak_ep = np.array(perr[:-self.nbkg_params]).reshape(npeaks,4) 
        
         
        if do_show:
            font1=13
            locator_value=0.5

            plt.figure(figsize=(17,5))
            ax1=plt.subplot(121)
            plt.plot(xx,yy,"*")
            plt.plot(xx,yyc,"-")
            plt.plot(xx,yyc-yy,"g-")
            plt.title('Fit, frame: %d, azimuthal range (%.1f$\degree$, %.1f$\degree$), $\chi_{center}=$%.1f$\degree$, ichi=%d' 
                      %(irow, chi_range[0], chi_range[1], (chi_range[1]-chi_range[0])/2+chi_range[0],ichi),fontdict={'fontsize': font1}) 
            plt.xlabel('q $[\AA^{-1}]$',fontdict={'fontsize':font1})
            plt.ylabel('Intensity',fontdict={'fontsize':font1})
            plt.grid(which='both',axis='both')
            loc = plticker.MultipleLocator(base=locator_value) # this locator puts ticks at regular intervals
            ax1.xaxis.set_major_locator(loc)

            ax2=plt.subplot(122)
            plt.semilogy(xx,yy,"*")
            plt.semilogy(xx,yyc,"-")

            #No peak lines
            #for i in range(0,npeaks):
            #    plt.axvline(x=peak_p[i][1],color='midnightblue',alpha=0.3)

            plt.title('Fit, frame: %d, azimuthal range (%.1f$\degree$, %.1f$\degree$), $\chi_{center}=$%.1f$\degree$, ichi=%d, do_fit=%r' 
                      %(irow, chi_range[0], chi_range[1], (chi_range[1]-chi_range[0])/2+chi_range[0],ichi, do_fit),fontdict={'fontsize': font1}) 
            plt.xlabel('q $[\AA^{-1}]$',fontdict={'fontsize':font1})
            plt.ylabel('log(Intensity)',fontdict={'fontsize':font1})
            plt.grid(which='both',axis='both')
            loc = plticker.MultipleLocator(base=locator_value) # this locator puts ticks at regular intervals
            ax2.xaxis.set_major_locator(loc)
            plt.tight_layout()
            plt.show()
        

        # The fitting function returns the parameter-matrix and its error-matrix.
        return peak_p, peak_ep, npeaks 
    
    def fitting_functions(self):
        
        # Prepare functions with parameters
        # In partial func-arg is predefined with self.fitting, but can be converted to an input argumented to try a different fitting routine,
        # see example 
        fittings=[partial(self.fitting,**params) for params in self.params_dicts]
        
        # Moving out the fittings block
     
        for fit in fittings:
            yield fit


    def multi_fitting(self, irows, chi_index=[0]):
           
        # Save the result, preparation
        nchi = self.nchi
        nimg = len(irows)
        
        # call once the fitting func to get npeaks, no fitting
        print("\033[33mDummy output\033[0m")
        dummy_result=self.fitting(irows[0], chi_index[0], ps1=False, tialn_ind=[],  ps2=True, ps3=True, ps4=True,  bkg_ho=True,  do_fit=False, do_show=False)
        # npeaks
        npeaks=dummy_result[2]
        print("\033[33m++++++++++++++++\033[0m"*3)
        
        # Allocate the storage matrix
        dset_p = np.zeros((nchi,nimg,npeaks,4),dtype=np.float)
        dset_p_ep = np.zeros((nchi,nimg,npeaks,4),dtype=np.float)
        print(dset_p.shape)
        print(dset_p_ep.shape)
        
        
        self._iter_irow(irows, chi_index, npeaks, dset_p, dset_p_ep)
        print("#######################################")
        print(dset_p)
        print("#######################################")
        return dset_p, dset_p_ep
        
    
    def _try_fittings(self, irow,  ichi, npeaks):
        """ Per"""
        for fit_func in self.fitting_functions():
            print("\nTrying fitting with : ", irow, ichi, fit_func)
            try:
                result = fit_func(irow, ichi)
                print("\033[92m----> Succeed to fit with: ", fit_func,  "\033[0m")
                return result , ichi
            except RuntimeError as e:
                print("\033[93m----> Fail to fit with: ", irow, ichi, e,"\n",fit_func, "\033[0m")
        return (([[np.nan, np.nan , np.nan, np.nan]]*npeaks, [[np.nan, np.nan , np.nan, np.nan]]*npeaks, npeaks)), ichi # TODO fix it


    def _iter_ichi(self, irow, irow_index, chi_index, npeaks, p_array, p_ep_array):
        for ichi in chi_index:
            print("\33[92m\n################\nICHI", ichi, "\033[0m")
            output, _ = self._try_fittings(irow, ichi, npeaks)
            p, p_ep, peaks = output
            p_array[ichi][irow_index] = p 
            p_ep_array[ichi][irow_index] = p_ep

            
    def _iter_irow(self, irows, chi_index, nbpeaks, p_array, p_ep_array):
        for irow_index, irow in enumerate(irows):        
            print("\n"+"\033[95m++++++++++++++++"*3)
            print('IROW', irow, "\033[0m")
            self._iter_ichi(irow, irow_index, chi_index, nbpeaks, p_array, p_ep_array)


    #create a proper output function
    #def save_fit_output(self,p,ep,sample_file,output_path, irows=None,chi_index=None,x_start=None, x_end=None, y_start=None, y_end=None, x_len=None):
    def save_fit_output(self,p,ep,sample_file,output_path, irows=None,chi_index=None,start_row=None, start_col=None, rows=None, cols=None, col_len=None, row_inc=1, col_inc=1):

        
        #timestamp=datetime.datetime.fromtimestamp(time()).isoformat().replace(":", "_")
        #new time stamp
        #no microseconds, and for a more compact version one could use replace(":","")
        timestamp=datetime.datetime.fromtimestamp(time()).replace(microsecond=0).isoformat().replace(":","_")        
        hdf5filename = os.path.join(output_path,"{}_F_{}_fit.h5".format(timestamp,sample_file[0:-3]))

        # Create hdf5setnames
        hdf5dsetname_fit_origin="origin"
        hdf5dsetname_fit_p="/entry/fit/p"
        hdf5dsetname_fit_ep="/entry/fit/ep"
        hdf5dsetname_fit_irows="/entry/fit/irows"
        hdf5dsetname_fit_chi_index="/entry/fit/chi_index"
        hdf5dsetname_get_irows_params="/entry/fit/get_ims_params"
        hdf5dsetname_q_range="/entry/fit/q_range"
        

        if all(v is not None for v in [(start_row, start_col, rows, cols, col_len)]):
        #if all(v is not None for v in [(x_start,x_end,y_start,y_end, x_len)]):
            #irows=self.get_irows(x_start,x_end,y_start,y_end, x_len)
            irows=self.get_ims(start_row, start_col, rows, cols, col_len, row_inc=1, col_inc=1)

        #if (x_start is None or x_start==[]):
        #    x_start=np.nan
        #    x_end=np.nan
        #    y_start=np.nan
        #    y_end=np.nan
        #    x_len=np.nan
            
        if (start_row is None or start_row==[]):
            star_row=np.nan
            start_col=np.nan
            rows=np.nan
            cols=np.nan
            col_len=np.nan    

        with h5py.File(hdf5filename, "w") as h5f: 
            grp = h5f.create_group('/entry/fit')
            h5f['/entry/fit'].attrs[hdf5dsetname_fit_origin]=sample_file
            h5f.create_dataset(hdf5dsetname_fit_p, data=p)
            h5f.create_dataset(hdf5dsetname_fit_ep, data=ep)
            h5f.create_dataset(hdf5dsetname_fit_irows, data=irows)
            h5f.create_dataset(hdf5dsetname_fit_chi_index, data=chi_index)
            h5f.create_dataset(hdf5dsetname_get_irows_params, data=np.asarray([start_row, start_col, rows, cols, col_len]))  
            #h5f.create_dataset(hdf5dsetname_get_irows_params, data=np.asarray([x_start, x_end, y_start, y_end,  x_len]))  
            if ((self.q_min is not None) and (self.q_max is not None)):
                h5f.create_dataset(hdf5dsetname_q_range, data=[self.q_min,self.q_max])
            else:
                h5f.create_dataset(hdf5dsetname_q_range, data=[np.nan,np.nan])
         