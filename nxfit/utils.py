import os
import h5py

# Class definition
class Read1DPilatusData:
    def __init__(self, data_input_file, output_path):
        
        # Store locals
        self.data_input_file = data_input_file
        # set output_path and remove trailing backslash
        self.output_path = os.path.normpath(output_path)
        
         # Separate file path from input file name
        self.filepath_input_file, self.filename = os.path.split(data_input_file)
        
         # Open the file and store number of data entries
        with h5py.File(self.data_input_file, 'r') as hf:
            self.data_keys = list(hf.keys())
            self.total_number_entries = len(self.data_keys)
        # Check that data file exist
        with h5py.File(self.data_input_file, 'r'):
            pass

    def data_read(self):
        
        with h5py.File(self.data_input_file,'r') as h5f:
            q_array = h5f["/entry/integ/q"][()]
            integration_array = h5f["/entry/integ/data"][()]
            sigma_array = h5f["/entry/integ/sigma"][()]
            azimuth_array =h5f[ "/entry/integ/azimuth_range"][()]
        
        return q_array, integration_array, sigma_array, azimuth_array

def get_irows(x_start, x_end, y_start, y_end,  x_len):
    output = []
    for i in range(y_start, y_end):
        y_offset = x_len * i
        for j in range(x_start + y_offset, x_end + y_offset):
            output.append(j)
    return output    
    
def get_ims(start_row, start_col, rows, cols, col_len, row_inc=1, col_inc=1):
    output = []
    for j in range(start_row, start_row+rows, row_inc):
        for i in range(start_col, start_col+cols, col_inc):
            im = j * col_len + i
            #print(im)
            output.append(im)
    return output
