#!/bin/bash
#
# job time, change for what your job requires
#SBATCH -t 1-00:00:00
#
# job name
#SBATCH -J integ
#
#SBATCH -N 1 --exclusive
#SBATCH --tasks-per-node=48
#SBATCH --mem=90GB
#
##SBATCH -p v100
##SBATCH --tasks-per-node=64
##SBATCH --mem=140GB
#
#
# filenames stdout and stderr - customise, include %j
#SBATCH -o res_integ_%j.out
#SBATCH -e res_integ_%j.err

export OMPI_MCA_mpi_warn_on_fork=0

set -o nounset

# write this script to stdout-file - useful for scripting errors
cat $0

# load the modules required for you program - customise for your program
source /home/gudlot/sw/sourceme_cpu.sh

export PYFAI_OPENCL=0

# get arguments
filename=$1

# run the program
# customise for your program name and add arguments if required
python /mxn/home/gudlot/jupyter_notebooks/Nanoindenter/integ/integ.py $filename

# Start as:
# sbatch j_integ.sh as_deposited_2nd_indent_load1_493mN/scan_0000_pil1m_0000.hdf5


# for 20190570/2019052208/raw/TiAlN_as_deposit
##azimuth_range=np.array([  0,  15,  30,  45,  90, 140,180, 242,  270, 285, 300, 315, 330, 345,np.nan])
##dchi=7.
# with /Si_calibration_20190525/'
# sbatch j_integ.sh TiAlN_as_deposit/scan_0007_pil1m_0000.hdf5



#same azimuth_range as above
# sbatch j_integ.sh TiAlN_as_deposit/scan_0008_pil1m_0000.hdf5
# sbatch j_integ.sh TiAlN_as_deposit/scan_0009_pil1m_0000.hdf5
# sbatch j_integ.sh TiAlN_as_deposit/scan_0010_pil1m_0000.hdf5
# sbatch j_integ.sh TiAlN_as_deposit/scan_0011_pil1m_0000.hdf5
# sbatch j_integ.sh TiAlN_as_deposit/scan_0012_pil1m_0000.hdf5
# sbatch j_integ.sh TiAlN_as_deposit/scan_0013_pil1m_0000.hdf5
# sbatch j_integ.sh TiAlN_as_deposit/scan_0014_pil1m_0000.hdf5
# sbatch j_integ.sh TiAlN_as_deposit/scan_0015_pil1m_0000.hdf5

# for 20190570/2019052208/raw/1000C
##azimuth_range=np.array([  0,  15,  30,  45,  90, 140,180, 242,  270, 285, 300, 315, 330, 345,np.nan])
##dchi=7.
# with /Si_calibration_20190525/'

# same azimuth_range as above and for all the same 

# sbatch j_integ.sh 1000C/scan_0007_pil1m_0000.hdf5
# sbatch j_integ.sh 1000C/scan_0008_pil1m_0000.hdf5
# sbatch j_integ.sh 1000C/scan_0009_pil1m_0000.hdf5
# sbatch j_integ.sh 1000C/scan_0010_pil1m_0000.hdf5
# sbatch j_integ.sh 1000C/scan_0011_pil1m_0000.hdf5
##sbatch j_integ.sh 1000C/scan_0012_pil1m_0000.hdf5  # I don't start 12,13 as they had problems...., at least not know...
##sbatch j_integ.sh 1000C/scan_0013_pil1m_0000.hdf5
# sbatch j_integ.sh 1000C/scan_0014_pil1m_0000.hdf5
# sbatch j_integ.sh 1000C/scan_0015_pil1m_0000.hdf5
# sbatch j_integ.sh 1000C/scan_0016_pil1m_0000.hdf5
# sbatch j_integ.sh 1000C/scan_0017_pil1m_0000.hdf5
# sbatch j_integ.sh 1000C/scan_0018_pil1m_0000.hdf5




#sbatch j_integ.sh TiAlN_as_deposit/scan_0007_pil1m_0000.hdf5