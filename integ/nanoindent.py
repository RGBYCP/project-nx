import os, copy, datetime, sys
import numpy as np
import h5py
import fabio, pyFAI

pyFAI.disable_opencl=True

from time import time

# Class definition
class PilatusData:
    def __init__(self, data_input_file, output_path, poni_file=None, mask_file=None):
        """ Creation of PilatusData class """
        # Store locals
        self.data_input_file = data_input_file
        # set output_path and remove trailing backslash
        self.output_path = os.path.normpath(output_path)
        self.mask = mask_file
        if self.mask:
            self.mask_data = fabio.open(self.mask).data
        self.poni = poni_file
        if self.poni:
            # This integrator uses ChiDiscAtPi (standard)
            self.aipi = pyFAI.load(self.poni)
            
            # Create the second-integrator with ChiDiscAtZero. 
            self.ai0 = copy.deepcopy(self.aipi)
            self.ai0.setChiDiscAtZero()
            
            
        # Separate file path from file name
        self.filepath_input_file, self.filename = os.path.split(data_input_file)
        # Open the file and store number of data entries
        with h5py.File(self.data_input_file, 'r') as hf:
            self.data_keys = list(hf.keys())
            self.total_number_entries = len(self.data_keys)
        # Check that data file exist
        with h5py.File(self.data_input_file, 'r'):
            pass
        # self.file_reader = file_reader(self.data_input_file)

    def data_read(self, entry_number):
        """ Open file and read data """
        
        # verify if entry_number is an integer:
        if not isinstance(entry_number,int):
            raise TypeError('entry_number must be an integer value.')  
            
        # confirm if given entry_number is in range
        if not entry_number in range(0,self.total_number_entries):
            raise ValueError("entry_number out of range.")    
        
        with h5py.File(self.data_input_file, 'r') as file_reader:
            return self._data_read(file_reader, entry_number)

    def _data_read(self, file_reader, entry_number):
        """ Read data from an open file """
        # returns one detector image
        h5data = file_reader.get(self.data_keys[entry_number] + '/measurement/Pilatus/data')
        return np.squeeze(np.array(h5data))

    def split_filename_extension(self, filename):
        """ Read data from an open file """
        filename_no_ext, _ = os.path.splitext(filename)
        return filename_no_ext

    def data_sum_up_all(self, generate_calibration2d_file=False):
        """ Sum all data up into one numpy array """

        # Allocate some space
        with h5py.File(self.data_input_file, 'r') as file_reader:
            pilatusdata_sum = self._data_read(file_reader, 0)
            
            # Check dimensions of one entry to distinguish between step- & flyscanning 
            
            # Version stepscanning
            if pilatusdata_sum.ndim==2:
                for i in range(1, self.total_number_entries):
                    pilatusdata_sum += self._data_read(file_reader, i)
                if generate_calibration2d_file:
                    filename_no_ext = self.split_filename_extension(self.filename)
                    print(filename_no_ext)
                    output_name = os.path.join(self.output_path,filename_no_ext + "_steps_summed.npy")
                    print(output_name)
                    np.save(output_name, pilatusdata_sum)
             
            # Version flyscanning
            elif pilatusdata_sum.ndim==3:
                                
                # Sum up first entry
                pilatusdata_sum=pilatusdata_sum.sum(axis=0)
                
                for i in range(1, self.total_number_entries):
                    pilatusdata_sum += self._data_read(file_reader, i).sum(axis=0)
                
                if generate_calibration2d_file:
                    filename_no_ext = self.split_filename_extension(self.filename)
                    print(filename_no_ext)
                    output_name = os.path.join(self.output_path,filename_no_ext + "_fly_summed.npy")
                    print(output_name)
                    np.save(output_name, pilatusdata_sum)
                            
        return pilatusdata_sum


    def data_integrate1d(self, pilatusdata, output_name=None, radial_range=None, azimuth_range=None, **kwargs):
        """ This function generates 1d integrated data.
            Input arguments:
            pilatusdata: A numpy array
            radial_range: select q-range
            azimuth_range: select azimuthal angle
            output_name: select a name for the output file #check for output path?

            Output:
            Returns: q/2th/r bins center positions and regrouped intensity (and error array if variance or variance model provided), uneless all==True.
            Return type: Integrate1dResult, dict
        """
        if not self.poni:
            raise ValueError("No poni file is defined, integration is impossible")
        if not self.mask:
            raise ValueError("No mask file is defined, integration is impossible")

            
        # Perform integrate1d
        if azimuth_range is not None:
                   
            chimin, chimax = azimuth_range
                
            if (chimin < 180 and chimax > 180):
                #print('data_integrate1d with chi: ', chimin,chimax)
                
                # Use in this case the integrator with ChiDiscAtZero
                res = self.ai0.integrate1d(
                    data=pilatusdata,
                    npt=1800,
                    filename=output_name,
                    unit="q_A^-1",  # unit="q_nm^-1",  #unit="2th_deg",
                    radial_range=radial_range,
                    azimuth_range=azimuth_range,
                    mask=self.mask_data,
                    correctSolidAngle=True,
                    error_model="poisson",
                    **kwargs)

            else:
                               
                # Use integrator with ChiDiscAtPi
                res = self.aipi.integrate1d(
                    data=pilatusdata,
                    npt=1800,
                    filename=output_name,
                    unit="q_A^-1",  # unit="q_nm^-1",  #unit="2th_deg",
                    radial_range=radial_range,
                    azimuth_range=azimuth_range,
                    mask=self.mask_data,
                    correctSolidAngle=True,
                    error_model="poisson",
                    **kwargs)
       
        else:
            #print('data_integrate1d with: ',azimuth_range)
            
            # Integration in the case for azimuth_range=None with ChiDiscAtPi
            
            res = self.aipi.integrate1d(
                data=pilatusdata,
                npt=1800,
                filename=output_name,
                unit="q_A^-1",  # unit="q_nm^-1",  #unit="2th_deg",
                radial_range=radial_range,
                azimuth_range=azimuth_range,
                mask=self.mask_data,
                correctSolidAngle=True,
                error_model="poisson",
                **kwargs)
            
        return res
        

    def data_integrate2d(self, pilatusdata, npt_rad, output_name=None, radial_range=None, azimuth_range=None, **kwargs):
        """ Calculate the azimuthal regrouped 2d image in q(nm^-1)/chi(deg) by default"""
        if not self.poni:
            raise ValueError("No poni file is defined, integration is impossible")
        if not self.mask:
            raise ValueError("No mask file is defined, integration is impossible")
        if not npt_rad:
            raise ValueError("Provide number of points in the radial direction")

        # Perform integrate2d
        return self.aipi.integrate2d(
            data=pilatusdata,
            npt_rad=npt_rad,
            npt_azim=3600,
            filename=output_name,
            correctSolidAngle=True,
            error_model="poisson",
            radial_range=radial_range,
            azimuth_range=azimuth_range,
            mask=self.mask_data,
            unit="q_A^-1",
            **kwargs)

    def sum_all_and_integrate1d(self, generate_calibration2d_file=False):
        
        output_filename = os.path.join(self.output_path, self.split_filename_extension(self.filename)+ "_sum_all.dat")
        sum_all = self.data_sum_up_all(generate_calibration2d_file)
        res = self.data_integrate1d(sum_all, output_name=output_filename)

        fig, ax = plt.subplots()
        plt.semilogy(res[0], res[1] / self.total_number_entries)  # I divide by the number of entries
        plt.title(self.data_input_file)  # self.filename
        # plt.xlabel('q (nm$^{-1}$)')
        plt.xlabel('2 theta (degrees)')
        plt.ylabel('Intensity')
        plt.grid(axis='both')

        majorLocator = MultipleLocator(5)
        majorFormatter = FormatStrFormatter('%d')
        minorLocator = MultipleLocator(1)

        ax.xaxis.set_major_locator(majorLocator)
        ax.xaxis.set_major_formatter(majorFormatter)
        ax.xaxis.set_minor_locator(minorLocator)

        # jupyter.plot1d(res)
        plt.show()

    def integrate1d_all(self, radial_range=None, azimuth_range=None, output_name=None, **kwargs):
        with h5py.File(self.data_input_file, 'r') as file_reader:
            return self._integrate1d_all(
                file_reader, 
                radial_range=radial_range, 
                azimuth_range=azimuth_range, 
                output_name=output_name,
                **kwargs)

    def _integrate1d_all(self, file_reader, radial_range=None, azimuth_range=None, output_name=None,**kwargs):

        # create general output filename
        output_filename = self.split_filename_extension(self.filename)
    
        # read the data for the first entry 
        pilatusdata = self._data_read(file_reader, 0)
        # integrate it, but no output should be generated, i.e. don't provide output_name or output_name=None
        res = self.data_integrate1d(pilatusdata, radial_range=radial_range, azimuth_range=azimuth_range, **kwargs)
        
         
        # create HDF file to store integrated data, this version is for fly scanning 
        hdf5filename = None
        hdf5dsetname_q="/entry/integ/q"
        hdf5dsetname = "/entry/integ/data"
        hdf5dsetname_sigma="/entry/integ/sigma"
        hdf5dsetname_origin='entry/integ/origin/'
        
        
        
        if pilatusdata.ndim == 3:
            # create output hdf5file (rewrite) 
            hdf5filename =  os.path.join(self.output_path,"{}_integ_fly.h5".format(output_filename))
            with h5py.File(hdf5filename, "w") as h5f:
                # save q
                h5f.create_dataset(hdf5dsetname_q, data=res[0])
                
                
                # create a dataset for the integrated data
                h5f.create_dataset(hdf5dsetname, shape=(self.total_number_entries, pilatusdata.shape[0], res[1].size),
                                   dtype=res[0].dtype,
                                   chunks=(self.total_number_entries, pilatusdata.shape[0], 10),
                                   compression="gzip")
                
        # create HDF file to store integrated data, this version is for a step scanning
        if pilatusdata.ndim == 2:
            
            # create output hdf5file (rewrite)
            hdf5filename =  os.path.join(output_path,"{}_integ_step.h5".format(output_filename))
      
            with h5py.File(hdf5filename, "w") as h5f:
                # change:
                h5f.crate_dataset(hdf5dsetname_origin, data=self.data_input_file)                
                
                #save q
                # good to know: as long as the radial_range is not changed, the q-range for various azimuth integrations remains the same.
                #h5f.create_dataset("/entry/integ/q", data=res[0]) 
               
                                
                # create a dataset for the q data  
                h5f.create_dataset(hdf5dsetname_q, shape=(self.total_number_entries, res[0].size),
                                               dtype=res[0].dtype,
                                               chunks=True, compression="gzip")
                

                #create a dataset for the integrated data
                h5f.create_dataset(hdf5dsetname, shape=(self.total_number_entries, res[1].size),
                                               dtype=res[1].dtype,
                                               chunks=True, compression="gzip")
                
                
                #create a dataset for the sigma data
                h5f.create_dataset(hdf5dsetname_sigma, shape=(self.total_number_entries, res[2].size),
                                               dtype=res[2].dtype,
                                               chunks=True, compression="gzip")
                
    
        
        for i in range(0, self.total_number_entries):

            # read in each entry of the data file
            pilatusdata = self._data_read(file_reader, i)
            
            # check what dimension the data entry has
            # Export 
            # A stepscan has dimension 2
            if pilatusdata.ndim == 2:
                                
                # to have still the choice if PyFAI integrate1d should generate an output file
                #if output_name is not None:             
                #    output_name = os.path.join(self.output_path,"{}_{:04d}.dat".format(output_filename, i))
                #    print("Generate data file %s" % (output_name))
                
                
                # integrate frame data in pilatusdata for a given radial_range and azimuth_range
                res = self.data_integrate1d(pilatusdata, radial_range=radial_range, azimuth_range=azimuth_range, output_name=None, **kwargs)
                
                # save integrated data to hdf5 file, q exists already (see above)
                with h5py.File(hdf5filename, "a") as h5f:
                        # q
                        h5f[hdf5dsetname_q][i, :] = res[0]
                        
                        # intensity
                        h5f[hdf5dsetname][i, :] = res[1]
                        
                        # intensity
                        h5f[hdf5dsetname_sigma][i, :] = res[2]
                
                if((i+1) % 50 ==0):    
                    print("DONE %d/%d" % (i+1,self.total_number_entries,))
                    sys.stdout.flush()
        
            
            # A flyscan has dimension 3
            elif pilatusdata.ndim == 3:
                # A flyscan has, for example, 50 entries, and in each entry contains 161 x 1043 x 981. 161 equals numbers of frames, 1043 x 981 is size of one detector image.
                
                # how many images are in one entry
                number_images_in_entry = pilatusdata.shape[0]

                # loop through the single images in one entry
                for n in range(number_images_in_entry):
                    
                    # to have still the choice if PyFAI integrate1d should generate an output file
                    #if output_name is not None: 
                    #    # create filename for output file
                    #    output_name = os.path.join(self.output_path , "{}_{:04d}_{:04d}.dat".format(output_filename, i, n))
                    #    print("Generate data file %s" % (output_name))
                   
                    # integrate frame data in pilatusdata for a given radial_range and azimuth_range
                    res = self.data_integrate1d(pilatusdata[n],radial_range=radial_range, azimuth_range=azimuth_range, output_name=None, **kwargs)
                    
                    # save integrated data to hdf5 file, q exists already (see above)
                    with h5py.File(hdf5filename, "a") as h5f:
                        h5f[hdf5dsetname][i, n, :] = res[1]
                    
                    if((n+1) % 50 ==0): 
                        print("DONE %d/%d" % (n+1,number_images_in_entry,))
                        sys.stdout.flush()
            else:
                raise ValueError("Data dimension is wrong.")
    
    def plot_2dimage(self,entry_number=None):
        """ This functions plots a 2D detector image.
            In the first image origin="lower" is specified, in the second image not.
            A default value is provided for the entry_number, choosing an image from the middle of the measurement.
        """
        
        if entry_number is None:
            entry_number=round(self.total_number_entries/2)
            
        
        # load np.array from an entry_number
        pilatusdata=self.data_read(entry_number)
        
        # load required function 
        from mpl_toolkits.axes_grid1 import make_axes_locatable

        # creat subplots
        fig2d, (ax1, ax2) = plt.subplots(1,2,figsize=(15,15*1.0632008154943935))

        # create image with origin="lower"
        im1=ax1.imshow(twod_image, norm=LogNorm(),cmap = 'inferno',origin="lower")
        ax1.set_title('Frame: %i, origin=lower' % (entry_number))
        # Create divider for existing axes instance
        divider1 = make_axes_locatable(ax1)
        # Append axes to the right of ax1, with 10% width of ax1
        cax1 = divider1.append_axes("right", size="10%", pad=0.1)
        # Create colorbar in the appended axes
        cbar1 = plt.colorbar(im1, cax=cax1)

         # create image where origin is not specified
        im2=ax2.imshow(twod_image, norm=LogNorm(),cmap = 'inferno')
        ax2.set_title('Frame: %i, no origin specified' % (entry_number))
        # Create divider for existing axes instance
        divider2 = make_axes_locatable(ax2)
        # Append axes to the right of ax2, with 10% width of ax2
        cax2 = divider2.append_axes("right", size="10%", pad=0.05)
        # Create colorbar in the appended axes
        cbar2= plt.colorbar(im2, cax=cax2)

        plt.show()

    def plot_radial_integration(self,entry_number=None):
        """ This functions plots a complete radial integration of a 2D detector image.
            A default value is provided for the entry_number, choosing an image from the middle of the measurement.
        """
        
        if entry_number is None:
            entry_number=round(self.total_number_entries/2)
         
        # load np.array from an entry_number
        pilatusdata=self.data_read(entry_number)
        
        # perform radial integration
        oned_data=self.data_integrate1d(pilatusdata)
            
        fig, ax1 = plt.subplots(1, 1, figsize=(12,6*1.0632008154943935))
     
        #ax1.plot(oned_data)
        ax1.plot(oned_data[0],oned_data[1])
        ax1.set(xlabel="'q $[\AA^{-1}]$'", ylabel="Intensity")
        ax1.title.set_text('Frame: %i, complete radial integration' % (entry_number))
        ax1.grid(True)
        plt.show()

    def get_scan_info(self):

        # Check out the scan
        print('Numbers of entries in a scan: {}'.format(self.total_number_entries))
        print('Frame numbers range from %i to %i.' % (range(self.total_number_entries)[0],range(self.total_number_entries)[-1]))
        
        # load one image
        # for simplicity 0th image is loaded
        pilatusdata=self.data_read(0)
                
        print('Dimension of one detector image: {}'.format(pilatusdata.ndim))
        if pilatusdata.ndim == 2:
            print('This is a step scan.')
        elif pilatusdata.ndim == 3:
            print('This is a fly scan.')
        else:
            raise ValueError("Unknown dimension.")

        print('Shape of one image: {}'.format(pilatusdata.shape))

        
        
    def create_output_filename_entry(self,entry_number,file_extension):
        """ This functions creates an output_filename (full path) for a given entry_number.
            entry_number: equals the frame number, integer, mandatory variable
            file_extension: a string, e.g. '.edf'
            
        """
        if not isinstance(file_extension, str):
            raise TypeError('file_extension must be a string.')
        
        # create output filename in one variable
        output_filename=os.path.join(self.output_path,os.path.splitext(self.filename)[0]+'_%s%s' % (self.data_keys[entry_number],file_extension))
                             
        print(output_filename)
        return output_filename
    

    def frame2edf(self, entry_number):
        """ This function exports one frame given by entry_number as edf file.            
        """       
        
        # Choose frame (=entry_number) and load it
        pilatusdata=self.data_read(entry_number)

        # creates a new instance of the class fabio.fabioimage.FabioImage and assigns this object to the local variable img.
        img=fabio.fabioimage.FabioImage(pilatusdata)
        #print(isinstance(img,fabio.fabioimage.FabioImage))

        # create output_filename for given entry_number, file_extension='.edf'
        output_filename=self.create_output_filename_entry(entry_number,file_extension='.edf')
  
        # export the np.array as edf
        res=img.convert("edf").save(output_filename)
        
        # check what is inside the output_folder
        print(os.listdir(self.output_path))
        
    def frame2tif(self, entry_number):
        """ This function exports one frame given by entry_number as tif file.            
        """  
        
        # Choose frame (=entry_number) and load it
        pilatusdata=self.data_read(entry_number)
        
        # creates a new instance of the class fabio.tifimage.TifImage and assigns this object to the local variable img.
        img=fabio.tifimage.TifImage(pilatusdata)
        
        # create output_filename for given entry_number, file_extension='.tif'
        output_filename=self.create_output_filename_entry(entry_number,file_extension='.tif')
        
        # export the np.array as tif
        img.write(output_filename)
        
        # check what is inside the output_folder
        print(os.listdir(self.output_path))
              
        
    def azimuth_integrate1d_all(self, azimuth_range, dchi, radial_range=None, output_name=None,  **kwargs):
        """ This function performs with the help of PyFAI integrate1d the azimuth integration for all frames in one measurement set.
            
            WORK IN PROGRESS
            
            In:
            radial_range: equals q, tth
            azimuth_values: a np.array containing the angles around which azimuthal integration
                            should be performed
                            or the standard pyFAI azimuth_range tuple = (float,float)
            dchi: half of the width of azimuthal_range, e.g. azimuth_range=(i-dchi,i+dchi), where i is one azimuth values (chi)
            output_name: if set, pyFAI will save the result as .dat, but it takes too much time as there are too many frames. Recommended value: None  
              
            Out:
        
            An h5 file with the data in a 3d array
    
        """
        # check if azimuth_range is np.array or a tuple
        # depending on the number of given ranges, I adjust zdim
        if not (isinstance(azimuth_range,tuple) or isinstance(azimuth_range,np.ndarray)):
            raise TypeError('azimuth_range should be a tuple or a np.ndarray')       
             
        # check if azimuth_range are in a certain range, i.e. azimuth_range [-180, 360]
        # PyFAI ignores values out of range
        if isinstance(azimuth_range,np.ndarray):
            if not azimuth_range.ndim==1:
                raise ValueError('azimuth_range as np.ndarray should have a ndim=1.')            
            if not ((azimuth_range >= -180).all() and (azimuth_range <= 360).all()):
                result=np.where(np.logical_not(np.logical_and(azimuth_range >= -180, azimuth_range <= 360)))
                if not np.isnan(azimuth_range[result]).all():
                    print('Wrong value(s)', azimuth_range[result])
                    raise ValueError('Value(s) out of range!')
                
        elif isinstance(azimuth_range, tuple):
            if not len(azimuth_range)==2:
                new_list = all( -180<=i<=360 for i in azimuth_range)
                if not new_list is True:
                    raise ValueError("Value out of range.")
                raise ValueError('Too many values given for azimuth_range as tuple. Only 2 values are allowed.')                

        # If azimuth_range is a tuple and a dchi is provided, the programm will produce the wrong output. 
        # The following code should prevent this. dchi is set to None if azimuth_range is a tuple.
        try:
            if (isinstance(azimuth_range,tuple) and dchi):
                raise ValueError    
        except ValueError:
            print("If azimuth_range is already as chi-range. dchi is set to None.")
            dchi=None

        # If azimuth_range is a np.ndarray with length 2 and dchi=None, azimuthal integration will work as usual in pyFAI
        # azimuth_range = chi range = azimuth_range[0],azimuth_range[1]
        
        # Programm will fail with azimuth_range as np.ndarray (azimuth_range.ndim=1) with more than 2 entries and dchi=None
        # Error message will be: too many values to unpack (expected 2) 
        if (isinstance(azimuth_range, np.ndarray) and len(azimuth_range)>2 and dchi is None):
            raise ValueError("If azimuth_range is a np.ndarray with len>2, a dchi is required.")
 

        # open the data file
        with h5py.File(self.data_input_file, 'r') as file_reader:
            return self._azimuth_integrate1d_all(
                file_reader, 
                radial_range=radial_range, 
                azimuth_range=azimuth_range, 
                output_name=output_name,
                dchi=dchi,
                **kwargs)
        
      
    def _azimuth_integrate1d_all(self, file_reader, radial_range=None, azimuth_range=None, dchi=None, **kwargs):
        # Get general output filename
        output_filename = self.split_filename_extension(self.filename)
        # Check zdim
        zdim = 1 if not dchi else len(azimuth_range)
        print('Check Zdim',zdim)
        print('Check:',len(azimuth_range))
        
        # Read the data for the first entry 
        pilatusdata = self._data_read(file_reader, 0)
        if not pilatusdata.ndim == 2:            
            raise ValueError('Currently not yet implemented.')
            
          
        # Integrate it, but no output should be generated, i.e. don't provide output_name or output_name=None.
        # Initialise the HDF5 file with an integration, where the azimuth_range=None.
        
        #res = self.data_integrate1d(pilatusdata, radial_range=radial_range, azimuth_range=azimuth_range, **kwargs) 
        res = self.data_integrate1d(pilatusdata, radial_range=radial_range, azimuth_range=None, **kwargs) 
        # Create file writer
        self._file_writer = StepFileWriter(output_filename, self.output_path, self.data_input_file)        
        
        # Setup hdf5 structure base on previous integration
        self._file_writer.init_datastructure(zdim=zdim, q_data=res[0], int_data=res[1], total_entries=self.total_number_entries, sigma_data=res[2])          
       
    # Integrate now all the entries
        for entry_number in range(0, self.total_number_entries):
            self._azimuth_integrate1d_one_entry(file_reader, entry_number, radial_range, azimuth_range, dchi, **kwargs)
            if((entry_number+1) % 10 ==0):    
                print("DONE %d/%d" % (entry_number+1,self.total_number_entries,))
                sys.stdout.flush()
    
  
    def _azimuth_integrate1d_one_entry(self, file_reader, entry_number, radial_range=None, azimuth_range=None, dchi=None, **kwargs):
        # Disable print out to run faster?
        #print("In one entry", file_reader, entry_number, radial_range, azimuth_range, dchi)
        
        # Load one entry
        pilatusdata = self._data_read(file_reader, entry_number)
        
        # If this is going to be extended for fly scanning, you have to open a case, if pilatusdata.ndim == 2: elif pilatusdata.ndim==3
        if not pilatusdata.ndim == 2:
            raise ValueError('Currently not yet implemented.')      
        if dchi:
            for azi_index, azi_value in enumerate(azimuth_range):
                # Prepare
                if (np.isnan(azi_value)):
                    chi_range = (0, 360)
                else:
                    chi_range = (azi_value - dchi, azi_value + dchi)
                
                #print(chi_range)
               
                self._azimuth_integrate1d_one_sector(
                    pilatusdata=pilatusdata, 
                    entry_number=entry_number, 
                    radial_range=radial_range, 
                    chi_range=chi_range, 
                    azi_index=azi_index,
                    **kwargs)
        
        # No dchi, use azimuth range as it is
        else:
            print("No dchi!")
            chi_range = azimuth_range
          
            # Python starts at 0, not at 1! azi_index=0 for dchi=None
            # No multiple chi-angle points expected. One azimuthal ingegration only.
            azi_index = 0
            self._azimuth_integrate1d_one_sector(
                pilatusdata=pilatusdata, 
                entry_number=entry_number,
                radial_range=radial_range, 
                chi_range=chi_range, 
                azi_index=azi_index,
                **kwargs)    
        
    
    def _azimuth_integrate1d_one_sector(self, pilatusdata, entry_number, radial_range, chi_range, azi_index, **kwargs):
        
        chimin, chimax = chi_range
                 
        # Proceed
        # Due to the discontinuity at pi, change the location of the discontinuity whenever 180 deg is in range.
        
        # Old code
        #if (chimin < 180 and chimax > 180):
        #    self.ai.setChiDiscAtZero() # change the location of the discontinuity
        #    res = self.data_integrate1d(pilatusdata, radial_range=radial_range,  azimuth_range=chi_range, **kwargs)
        #    self.ai.setChiDiscAtPi()   # back to standard
        #else:
        #   
        #    res = self.data_integrate1d(pilatusdata, radial_range=radial_range, azimuth_range=chi_range, **kwargs)  
        
        # New code. Instead of switching the ChiDisc back and forth, I change the integrator, but this is done in self.data_integrated1d
        res = self.data_integrate1d(pilatusdata, radial_range=radial_range,  azimuth_range=chi_range, **kwargs)
            
           
        # Saving
        # I save here q, int, and sigma
        self._file_writer.append(q_data=res[0], int_data=res[1], chimin=chimin, chimax=chimax, 
                                 azi_index=azi_index, entry_number=entry_number,sigma_data=res[2])

    def plot_azimuth_integ(self, frame, azimuth_values, dchi, radial_range=None):
        """
        Plots the azimuthal integration for one frame for given azimuth_values and dchi

        frame: frame number 
        dchi: half the width of the azimuthal range
        radial_range: tuple (begin,end) in Angström

        azimuth_values: np.array with center points of the sectors for azimuthal integration
        
        """

        # Select one frame
        if not isinstance(frame,int):
            raise TypeError('azimuth_values needs to be np.array.')
        else:   
            twod_image=self.data_read(frame)
        
        if not isinstance(dchi, (int, float)):
            raise TypeError('Wrong type')
        if not dchi>=0:
            raise ValueError('dchi needs to be larger than 0.')

        if not isinstance(radial_range,tuple):
            raise TypeError('radial_range needs to be a tuple.')
        # Define q range, radial range
        #radial_range=(0,7) #q in Angström

        # These are angles for the azimuthal integration
        #azimuth_values=np.array([0,45,135,225,270,-90, 315]) 
        #azimuth_values=np.array([0,20,45,90,135,180,225,270,-90, 315])
        #azimuth_values=np.array([0,22.5,45,90,135, 180,225,245, 270, 315, 337.5])
        #Special one for the corner
        #azimuth_values=np.array([-115.20830359 , 138.12178433  , 25.97897445 , -49.08221011])

        #Ensure that azimuth_values is np.array
        if not isinstance(azimuth_values, np.ndarray):
            raise ValueError('azimuth_values needs to be np.array.')


        # 0.5 width of chi, chi=2*dchi in total
        #dchi=7.5


        # Prepare number of plots
        number_azimuth_values=len(azimuth_values)
        rows = 4
        cols = number_azimuth_values // rows + 1

        def trim_axs(axs, N):
            """little helper to massage the axs list to have correct length..."""
            axs = axs.flat
            for ax in axs[N:]:
                ax.remove()
            return axs[:N]

        # Prepare the plot
        figw,figh=50,50*1.0632008154943935
        f, axs = plt.subplots(rows, cols,squeeze=True,figsize=(figw,figh))
        # Adjust the subplots
        plt.subplots_adjust(left=1/figw, right=1-1/figw, bottom=1/figh, top=1-1/figh,wspace=figw/500, hspace=figh/100)
        font1=35

        axs = trim_axs(axs, len(azimuth_values))


        # Create an empty array
        integration_array=np.zeros((len(azimuth_values),1800))
        q_array=np.zeros((len(azimuth_values),1800))
        sigma_array=np.zeros((len(azimuth_values),1800))
        azimuth_array=np.zeros((len(azimuth_values),2))

        # Execute integration
        for ax,m, i in zip(axs, range(0,len(azimuth_values)), azimuth_values):

            # set azimuth_range for each iteration, azimuth angle in degrees
            azimuth_range=(i-dchi,i+dchi) 

            # Perform azimuthal integration
            azimuth_image=self.data_integrate1d(twod_image,radial_range=radial_range,azimuth_range=azimuth_range)

            # Store result (integrated intensity) in the array
            integration_array[m,:]=azimuth_image[1]
            q_array[m,:]=azimuth_image[0]
            sigma_array[m,:]=azimuth_image[2]
            azimuth_array[m,:]=azimuth_range


            ax.plot(azimuth_image[0],azimuth_image[1], '-', linewidth=3, markersize=12)

            ax.set_title("Azimuthal integration in \n radial range (%d,%d) $[\AA^{-1}]$,\n azimuthal range (%.1f$\degree$, %.1f$\degree$), $\chi_{center}=$%.1f$\degree$" 
                         %(radial_range[0],radial_range[1], azimuth_range[0],azimuth_range[1],i),fontdict={'fontsize': font1})

            ax.tick_params(labelsize=25)
            ax.set_xlabel('q $[\AA^{-1}]$',fontdict={'fontsize':font1})
            ax.set_ylabel('Intensity',fontdict={'fontsize':font1})

            ax.grid(True) 

        plt.show()

        print(type(integration_array))
        print(type(q_array))
        print(integration_array.shape)
        print(q_array.shape)


        #Check if q_values are equal
        print(np.array_equal(q_array[0,:],q_array[1,:]))
        
         # Create hdf5setnames
        hdf5dsetname_integ_origin="filename"
        hdf5dsetname_inteq_q="/entry/integ/q"
        hdf5dsetname_integ_data="/entry/integ/data"
        hdf5dsetname_inteq_sigma="/entry/integ/sigma"
        hdf5dsetname_inteq_azimuth_range="/entry/integ/azimuth_range"

        # save data
        with h5py.File(output_path+'frame_'+str(frame)+'_azimuth_integ.h5','w') as h5f:
            grp = h5f.create_group('/entry/origin')
            h5f['/entry/origin'].attrs[hdf5dsetname_integ_origin]=sample_file
            h5f.create_dataset(hdf5dsetname_inteq_q, data=q_array)
            h5f.create_dataset(hdf5dsetname_integ_data, data=integration_array)
            h5f.create_dataset(hdf5dsetname_inteq_sigma, data=sigma_array)
            h5f.create_dataset(hdf5dsetname_inteq_azimuth_range, data=azimuth_array)

        print('Finished')        
        

        
class StepFileWriter:
    # Saves integrated data for step scans
    
    def __init__(self, output_filename, output_path,data_input_file):
        # Store locals
        self.output_filename = output_filename
        self.output_path = output_path
        self.data_input_file=data_input_file
        # Setup h5 structure
        self.hdf5filename = None
        # Setname for the q data
        self.hdf5dsetname_q="/entry/integ/q"
        # Setname for the interaged data
        self.hdf5dsetname = "/entry/integ/data"
        # Setname for the sigma data
        self.hdf5dsetname_sigma = "/entry/integ/sigma"
        # Setname for the azimuth values
        self.hdf5dsetname_azimuth = "/entry/integ/azimuth_range"
        # Setname for the file with data of origin
        self.hdf5dsetname_origin='origin/'
                
        # Create output hdf5file (rewrite)
        self.hdf5filename = os.path.join(
            # Old version
            #output_path, "{}_{}_integ_step.h5".format(datetime.datetime.fromtimestamp(time()).isoformat().replace(":", "_"), output_filename)
            
            # New version, no microseconds
            output_path, "{}_{}_integ_step.h5".format(datetime.datetime.fromtimestamp(time()).replace(microsecond=0).isoformat().replace(":", "_"), output_filename)
            
            
            
        )

    def init_datastructure(self, zdim, q_data, int_data, total_entries, sigma_data=None):
        with h5py.File(self.hdf5filename, "w") as h5f: 
            
            # Save origin, where does the data comes from?
            grp = h5f.create_group('/entry/fit')
            h5f['/entry/fit'].attrs[self.hdf5dsetname_origin]=self.data_input_file
            
            
            # Save q value
            #h5f.create_dataset("/entry/integ/q", data=q_data)
            h5f.create_dataset(
                self.hdf5dsetname_q, 
                shape=(total_entries, q_data.size, zdim),
                dtype=q_data.dtype,
                chunks=True,
                compression="gzip",
            )
                
            # Create a dataset for integrated data
            h5f.create_dataset(
                self.hdf5dsetname,
                shape=(total_entries, int_data.size, zdim),
                dtype=int_data.dtype,
                chunks=True,
                compression="gzip",
            )
            
            
            # Save azimuth range (comment: check later the dtype)
            h5f.create_dataset(
                self.hdf5dsetname_azimuth,
                shape=(total_entries, 2, zdim),
                dtype=np.float64,
                chunks=True,
                compression="gzip",
            )
            
            if sigma_data is not None:
                # Create a dataset for the sigma data
                h5f.create_dataset(
                self.hdf5dsetname_sigma,
                shape=(total_entries, sigma_data.size, zdim),
                dtype=sigma_data.dtype,
                chunks=True,
                compression="gzip",
            )
                
                

    def append(self, q_data, int_data, chimin, chimax, azi_index, entry_number, sigma_data=None):
        
        #print('Saving: ', chimin, chimax, azi_index, entry_number)
        
        with h5py.File(self.hdf5filename, "a") as h5f:
            h5f[self.hdf5dsetname_q][entry_number, :, azi_index] = q_data
            h5f[self.hdf5dsetname][entry_number, :, azi_index] = int_data
            if sigma_data is not None:
                h5f[self.hdf5dsetname_sigma][entry_number, :, azi_index] = sigma_data
                
            h5f[self.hdf5dsetname_azimuth][entry_number, 0, azi_index] = chimin
            h5f[self.hdf5dsetname_azimuth][entry_number, 1, azi_index] = chimax
            