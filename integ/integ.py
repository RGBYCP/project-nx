# Imports
import numpy as np
import os, time, sys
from time import time


from nanoindent import *

# --- Define CALIBRATION ---

## Provide path to process folder that equals the output path
#path_process='/data/visitors/nanomax/20190570/2019041008/process/Si_calibration_20190415/'
## Load the PONI file and the mask file
#poni='PONI_Si_calibration_20190415.poni'  
###mask='Mask_Si_calibration_20190415.edf' #don't use anymore
#mask='Mask_Si_calibration_20190919.edf'  #updated with dead pixels
#poni_file=path_process+poni
#mask_file=path_process+mask



# Provide path to process folder that equals the output path
path_process='/data/visitors/nanomax/20190570/2019052208/process/Si_calibration_20190525/'     #for 20190570/2019052208/raw/TiAlN_as_deposit & 1000C 
# Load the PONI file and the mask file
poni='PONI_Si_calibration_20190525.poni' 
mask='Mask_Si_calibration_20190525_20190921.edf' 
poni_file=path_process+poni
mask_file=path_process+mask


# --- Define DATA IN/OUT ---

# Provide sample path and output path
#sample_path='/data/visitors/nanomax/20190570/2019041008/raw/'
#sample_file=sys.argv[1] # 'scan_0000_pil1m_0000.hdf5'
#source=sample_path+sample_file
#output_path='/data/visitors/nanomax/20190570/2019041008/process/'

sample_path='/data/visitors/nanomax/20190570/2019052208/raw/'
sample_file=sys.argv[1]
source=sample_path+sample_file
output_path='/data/visitors/nanomax/20190570/2019052208/process/'

sys.stdout.flush()

# Modification necessary due to script
mod_output_path=output_path+sample_file
output_path,file_name=os.path.split(mod_output_path)

sys.stdout.flush()
print("--------------------     Final output_path  --------------------")
print('Path: ', output_path)



# This cell performs the azimuthal integration.

# Start timer
start_time = time()

# Prepare a sample scan
asdep_noload=PilatusData(source,output_path,poni_file=poni_file,mask_file=mask_file)

# Choose azimuth angles (center points) and the dchi-range
#azimuth_range=(260,290)
#azimuth_range=np.array([0,90,180,270,-90])

# note: np.nan means 360 deg integration

#azimuth_range=np.array([0,45,75,90,135,180,225,270,-90, 315, np.nan])
azimuth_range=np.array([  0,  15,  30,  45,  90, 140,180, 242,  270, 285, 300, 315, 330, 345,np.nan])   #for 20190570/2019052208/raw/TiAlN_as_deposit & # for 20190570/2019052208/raw/1000C
dchi=7.5


#x=np.array(np.linspace(0,360,25))
#x[-1]=np.nan
#azimuth_range=x
#print(azimuth_range)
#dchi=7.5


# Let's go
print('....Let\'s go and integrate.....')



# Executing azimuthal integration
# Currently commented out to avoid re-execution

asdep_noload.azimuth_integrate1d_all(azimuth_range, dchi)

# Stop timer
elapsed_time = time() - start_time
print(elapsed_time)

print('Finished!')
sys.stdout.flush()